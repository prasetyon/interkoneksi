@extends('app')
@section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
          <div class="col-lg-3 col-md-6 col-sm-12 col-12">
            <h3>Bandingkan DB</h3>
          </div>
          <div class="col-lg-9 col-md-6 col-sm-0 col-0">
          </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
          <form method="post" action="">
            @csrf
            <div class="row">
              <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                  <select class="form-control select2" name="cat1" style="width: 100%;" onChange="this.form.submit();">
                    @foreach($data['category'] as $c)
                      <option value="{{$c->db}}"
                        @if($data['selected1']==$c->db) selected
                        @elseif($data['selected2']==$c->db) disabled='disabled'
                        @endif>{{$c->name}}
                      </option>
                    @endforeach
                  </select>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                  <select class="form-control select2" name="cat2" style="width: 100%;" onChange="this.form.submit();">
                    @foreach($data['category'] as $c)
                      <option value="{{$c->db}}"
                        @if($data['selected2']==$c->db) selected
                        @elseif($data['selected1']==$c->db) disabled='disabled'
                        @endif>{{$c->name}}
                      </option>
                    @endforeach
                  </select>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
      &nbsp;
      @if(isset($state) && $state!='summary')
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6">
              <h3>Info {{$state}}</h3>
            </div>
            <div class="col-md-6 col-sm-6">
            <button type="button" class="btn btn-block btn-info btn-flat" style="width:auto; float:right" onclick="syncTable('{{$state}}');"
              data-toggle="tooltip" title="Full Sync {{$state}}">Synchronize {{$state}}</button>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-valign-middle">
                    <thead bgcolor="#bdbdbd">
                        <tr>
                            <th>State</th>
                            <th>{{$data['selected1']}}</th>
                            <th>{{$data['selected2']}}</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($data['res'] as $res)
                        @php $ket = explode('|', $res->ket); @endphp
                        <tr>
                            <td>{{$res->name}} </td>
                            <td>
                                @if(isset($res->t1data))
                                <div class="modal fade" id="{{'modalt1'.$res->id}}">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">{{$data['selected1']}} Diff Data</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="card-body table-responsive p-0">
                                                    <table class="table table-bordered table-valign-middle">
                                                        <thead bgcolor="#f0f0f0">
                                                            <tr>
                                                                @foreach($res->column as $col)
                                                                <th>{{$col}}</th>
                                                                @endforeach
                                                                <th>Pilih</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                          <form method="POST" action="{{route($res->t1diff)}}" id="{{$res->t1diff}}">
                                                            @csrf
                                                            <input name="db1" value="{{$data['selected1']}}" hidden></input>
                                                            <input name="db2" value="{{$data['selected2']}}" hidden></input>
                                                            <input name="table" value="{{$state}}" hidden></input>
                                                            @php $ii=0; @endphp
                                                            @foreach($res->t1data as $dt)
                                                            <tr>
                                                                @foreach($res->column as $col)
                                                                  <td>{{ $dt->$col }}</td>
                                                                @endforeach
                                                                <input name="checked[]" value="{{$dt->keyz}}" hidden></input>
                                                                <td style="text-align: center; vertical-align:middle;">
                                                                  <div class="icheck-primary d-inline" >
                                                                    <input style="text-align:center; vertical-align:middle" name="{{$res->cb1}}[{{$ii}}]" type="hidden" value="0" >
                                                                    <input style="text-align:center; vertical-align:middle" name="{{$res->cb1}}[{{$ii++}}]" type="checkbox" value="1" >
                                                                  </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                          </form>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-info" onclick="document.getElementById('{{$res->t1diff}}').submit();"
                                              data-toggle="tooltip" title="Samakan baris terpilih pada {{$data['selected2']}} dengan {{$data['selected1']}}">Insert ke {{$data['selected2']}}</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <a style="color: #0000FF" data-toggle="modal" data-target="#{{'modalt1'.$res->id}}">{{$res->t1}}</a>
                                @else
                                {{$res->t1}}
                                @endif
                            </td>
                            <td>
                                @if($ket[0] == 't2')
                                    <small class="text-{{$ket[1]}} mr-1">
                                    <i class="fas fa-arrow-{{$ket[2]}}"></i>
                                    {{$ket[3]}}
                                    </small>
                                @endif
                                @if(isset($res->t2data))
                                <div class="modal fade" id="{{'modalt2'.$res->id}}">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">{{$data['selected1']}} Diff Data</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="card-body table-responsive p-0">
                                                    <table class="table table-bordered table-valign-middle">
                                                        <thead bgcolor="#bdbdbd">
                                                            <tr>
                                                                @foreach($res->column as $col)
                                                                <th>{{$col}}</th>
                                                                @endforeach
                                                                <th>Pilih</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                          <form method="POST" action="{{route($res->t2diff)}}" id="{{$res->t2diff}}">
                                                            @csrf
                                                            <input name="db1" value="{{$data['selected2']}}" hidden></input>
                                                            <input name="db2" value="{{$data['selected1']}}" hidden></input>
                                                            <input name="table" value="{{$state}}" hidden></input>
                                                            @php $ii=0; @endphp
                                                            @foreach($res->t2data as $dt)
                                                            <tr>
                                                                @foreach($res->column as $col)
                                                                  <td>{{ $dt->$col }}</td>
                                                                @endforeach
                                                                <input name="checked[]" value="{{$dt->keyz}}" hidden></input>
                                                                <td style="text-align: center; vertical-align:middle;">
                                                                  <div class="icheck-primary d-inline" >
                                                                    <input style="text-align:center; vertical-align:middle" name="{{$res->cb2}}[{{$ii}}]" type="hidden" value="0" >
                                                                    <input style="text-align:center; vertical-align:middle" name="{{$res->cb2}}[{{$ii++}}]" type="checkbox" value="1" >
                                                                  </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                          </form>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-info" onclick="document.getElementById('{{$res->t2diff}}').submit();"
                                              data-toggle="tooltip" title="Samakan baris terpilih pada {{$data['selected2']}} dengan {{$data['selected1']}}">Insert ke {{$data['selected1']}}</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <a style="color: #0000FF" data-toggle="modal" data-target="#{{'modalt2'.$res->id}}">{{$res->t2}}</a>
                                @else
                                {{$res->t2}}
                                @endif
                            </td>
                        </tr>
                      @endforeach
                      <tr>
                        @php $ket = explode('|', $nom->ket); @endphp
                            <td>{{$nom->name}} </td>
                            <td></td>
                            <td>
                                @if($ket[0] == 't2')
                                    <small class="text-{{$ket[1]}} mr-1">
                                    <i class="fas fa-arrow-{{$ket[2]}}"></i>
                                    {{$ket[3]}}
                                    </small>
                                @endif
                                @if(isset($nom->t2data))
                                <div class="modal fade" id="{{'modalt2'.$nom->id}}">
                                    <div class="modal-dialog modal-xl" style="width:100%;max-width:1500px">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">{{$data['selected1']}} Diff Data</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="card-body table-responsive p-0">
                                                    <table class="table table-bordered table-valign-middle">
                                                        <thead bgcolor="#bdbdbd">
                                                            <tr>
                                                                @foreach($nom->column as $col)
                                                                <th>{{$col}}</th>
                                                                @endforeach
                                                                <th>Pilih</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                          <form method="POST" action="{{route($nom->t2diff)}}" id="{{$nom->t2diff}}">
                                                            @csrf
                                                            <input name="db1" value="{{$data['selected1']}}" hidden></input>
                                                            <input name="db2" value="{{$data['selected2']}}" hidden></input>
                                                            <input name="table" value="{{$state}}" hidden></input>
                                                            <input name="statenomsync" id="statenomsync" hidden>
                                                            @php $ii=0; @endphp
                                                            @foreach($nom->t2data as $dt)
                                                            <tr>
                                                                @foreach($nom->column as $col)
                                                                  <td>
                                                                    @if(is_array($dt->{$col}))
                                                                      @php $xxz = 1; @endphp
                                                                      @foreach($dt->{$col} as $ddz)
                                                                        {{$xxz++.'. '.$ddz}}<br/>
                                                                      @endforeach
                                                                    @else {{ $dt->$col }}
                                                                    @endif
                                                                  </td>
                                                                @endforeach
                                                                <input name="checked[]" value="{{$dt->keyz}}" hidden></input>
                                                                <td style="text-align: center; vertical-align:middle;">
                                                                  <div class="icheck-primary d-inline" >
                                                                    <input style="text-align:center; vertical-align:middle" name="{{$nom->cb2}}[{{$ii}}]" type="hidden" value="0" >
                                                                    <input style="text-align:center; vertical-align:middle" name="{{$nom->cb2}}[{{$ii++}}]" type="checkbox" value="1" >
                                                                  </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                          </form>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-warning" onclick="submitSyncNom(1, '{{$nom->t2diff}}');"
                                              data-toggle="tooltip" title="Samakan baris terpilih pada {{$data['selected2']}} dengan {{$data['selected1']}}">Sync ke {{$data['selected1']}}</button>
                                            <button type="button" class="btn btn-info" onclick="submitSyncNom(0, '{{$nom->t2diff}}');"
                                              data-toggle="tooltip" title="Samakan baris terpilih pada {{$data['selected2']}} dengan {{$data['selected1']}}">Sync ke {{$data['selected2']}}</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <a style="color: #0000FF" data-toggle="modal" data-target="#{{'modalt2'.$nom->id}}">{{$nom->t2}}</a>
                                @else
                                {{$nom->t2}}
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
      @endif
      &nbsp;
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-6">
            <h3>Ringkasan</h3>
          </div>
          <div class="col-md-6 col-sm-6 col-6">
            <button type="button" class="btn btn-block btn-info btn-flat" data-toggle="tooltip" title="Full sync table"
              style="width:auto; float:right;" onclick="openTableModal();">Synchronize</button>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-valign-middle">
                    <thead bgcolor="#bdbdbd">
                        <tr>
                            <th rowspan="2" style="text-align: center">Table</th>
                            <th colspan="2" style="text-align: center">Last Updated</th>
                            <th colspan="2" style="text-align: center">Data Count</th>
                            <th rowspan="2" style="text-align: center">Show</th>
                            <th rowspan="2" style="text-align: center">Pilih</th>
                        </tr>
                        <tr>
                            <th style="text-align: center">{{$data['selected1']}}</th>
                            <th style="text-align: center">{{$data['selected2']}}</th>
                            <th style="text-align: center">{{$data['selected1']}}</th>
                            <th style="text-align: center">{{$data['selected2']}}</th>
                        </tr>
                    </thead>
                    <tbody>
                      @if(isset($data['data']) && count($data['data'])>0)
                      @foreach($data['data'] as $res)
                      <form method="post" action="" id="{{'form'.$res->table}}">
                        @csrf
                        <input name="table" value="{{$res->tbl}}" hidden>
                      </form>
                        <tr @if(isset($state) && $state==$res->tbl) bgcolor="#8ee7e8" @endif>
                            <td> {{$res->table}} </td>
                            <td> {{$res->updated1}} </td>
                            <td>
                              @if($res->interval > 0)
                                <small class="text-danger mr-1">
                                  <i class="fas fa-arrow-down"></i>
                                  {{abs($res->interval)}}
                                </small>
                              @elseif($res->interval < 0)
                                <small class="text-danger mr-1">
                                  <i class="fas fa-arrow-up"></i>
                                  {{abs($res->interval)}}
                                </small>
                              @else
                                <small class="text-success mr-1">
                                  OK
                                </small>
                              @endif
                              {{$res->updated2}} </td>
                            <td> {{$res->count1}} </td>
                            <td>
                              @if($res->count2 < $res->count1)
                                <small class="text-danger mr-1">
                                  <i class="fas fa-arrow-down"></i>
                                  {{$res->count1 - $res->count2}}
                                </small>
                              @elseif($res->count2 > $res->count1)
                                <small class="text-danger mr-1">
                                  <i class="fas fa-arrow-up"></i>
                                  {{$res->count2 - $res->count1}}
                                </small>
                              @else
                                <small class="text-success mr-1">
                                  <i class="fas fa-arrow-right"></i>
                                  OK
                                </small>
                              @endif
                              {{$res->count2}}
                            </td>
                            <td style="text-align: center; vertical-align:middle;">
                              <div class="icheck-primary d-inline" >
                                <input style="text-align:center; vertical-align:middle" type="checkbox" id="{{'checkbox'.$res->tbl}}"
                                  onchange="document.getElementById('{{'form'.$res->table}}').submit();"
                                  value="{{$res->tbl}}" @if(isset($state) && $state==$res->tbl) checked @endif>
                              </div>
                            </td>
                            <td style="text-align: center; vertical-align:middle;">
                              <div class="icheck-primary d-inline" >
                                <input style="text-align:center; vertical-align:middle" type="checkbox" id="{{'sync'.$res->tbl}}" onchange="checkSelected('{{$res->tbl}}');">
                              </div>
                            </td>
                        </tr>
                      @endforeach
                      @else
                        <td colspan="7" style="text-align: center">Tidak ada tabel dalam kategori</td>
                      @endif
                    </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>

      <!-- /.modal -->
        <div class="modal fade" id="modaltable">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Sinkronisasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="{{route('multisync')}}" id="syncTable">
                        @csrf
                        <input name="db1ts" value="{{$data['selected1']}}" hidden>
                        <input name="db2ts" value="{{$data['selected2']}}" hidden>
                        <input name="statetablesync" id="statetablesync" hidden>
                        <input name="tablesync" id="tablesync" hidden>
                        <a id="tableconfirm"></a>
                      </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button class="btn btn-warning" type="button" onclick="submitSyncTable(1);"
                          data-toggle="tooltip" title="Samakan tabel terpilih pada {{$data['selected1']}} dengan {{$data['selected2']}}">Sync ke {{$data['selected1']}}</button>
                        <button class="btn btn-primary" type="button" onclick="submitSyncTable(0);"
                          data-toggle="tooltip" title="Samakan tabel terpilih pada {{$data['selected2']}} dengan {{$data['selected1']}}">Sync ke {{$data['selected2']}}</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
      &nbsp;
    </div>
    <!-- /.content -->
@endsection


<!-- OPTIONAL SCRIPTS -->
<script>
  var sync = "";
  function checkSelected(tbl) {
    var cb = document.getElementById("sync"+tbl).checked;
    if(cb) sync += tbl+'|'
    else sync = sync.replace(tbl+'|','')
    sync = sync.replace('undefined','')
    document.getElementById("tablesync").value = sync;
    console.log(sync)
    document.getElementById("tableconfirm").textContent = 'Anda akan melakukan sinkronisasi pada tabel '+sync.replace(/\|/g, ', ');
  }

  function syncTable(tbl) {
    sync = tbl+'|'
    document.getElementById("tablesync").value = sync;
    document.getElementById("tableconfirm").textContent = 'Anda akan melakukan sinkronisasi pada tabel '+sync;
    openTableModal();
  }

  function submitSyncTable(val) {
    document.getElementById("statetablesync").value = val;
    document.getElementById("syncTable").submit();
  }

  function submitSyncNom(val, form) {
    document.getElementById("statenomsync").value = val;
    document.getElementById(form).submit();
  }

  function submitSyncTable(val) {
    document.getElementById("statetablesync").value = val;
    document.getElementById("syncTable").submit();
  }

  function openTableModal() {
    $("#modaltable").modal()
  }
</script>