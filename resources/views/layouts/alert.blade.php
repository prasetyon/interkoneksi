@if(session('alert-success'))
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class='alert alert-success' id="alert">
                    <h4><i class='icon fa'></i> {{session('alert-success')}}</h4>
                </div>
            </div>
        </div>
    </div>
@elseif(session('alert-danger'))
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class='alert alert-danger' id="alert">
                    <h4><i class='icon fa'></i> {{session('alert-danger')}}</h4>
                </div>
            </div>
        </div>
    </div>
@endif
