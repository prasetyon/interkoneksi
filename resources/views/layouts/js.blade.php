<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('public/AdminLTE3/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('public/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Bootstrap Switch -->
<script src="{{ asset('public/AdminLTE3/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('public/AdminLTE3/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/AdminLTE3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/AdminLTE3/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('public/AdminLTE3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('public/AdminLTE3/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('public/AdminLTE3/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- AdminLTE -->
<script src="{{ asset('public/AdminLTE3/dist/js/adminlte.js') }}"></script>

<script>
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
});

$("#example1").DataTable({
    "responsive": true,
    "autoWidth": false,
    "paging": false,
});

$("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch();
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>