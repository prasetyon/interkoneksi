<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('public/AdminLTE3/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
        <span class="brand-text font-weight-light">ITX {{Session::get('year')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                <a href="{{url('/')}}" class="nav-link @if($menu=='sync') active @endif">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Sync
                </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{url('scheduler')}}" class="nav-link @if($menu=='sched') active @endif">
                    <i class="nav-icon fas fa-clock"></i>
                    <p>
                        Scheduler Krisna-DB Ref
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{url('scheduler2')}}" class="nav-link @if($menu=='sched2') active @endif">
                    <i class="nav-icon fas fa-clock"></i>
                    <p>
                        Scheduler DB Ref Dev-Prod
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{url('schedinter')}}" class="nav-link @if($menu=='schedinter') active @endif">
                    <i class="nav-icon fas fa-clock"></i>
                    <p>
                        Scheduler Internal
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{url('mapping')}}" class="nav-link @if($menu=='mapping') active @endif">
                    <i class="nav-icon fas fa-table"></i>
                    <p>
                        Mapping Source
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{url('log')}}" class="nav-link @if($menu=='log') active @endif">
                    <i class="nav-icon fas fa-history"></i>
                    <p>
                        Log Sync
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{url('logout')}}" class="nav-link">
                    <i class="nav-icon fas fa-sign-out"></i>
                    <p>
                        Logout
                    </p>
                </a>
            </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
