<!DOCTYPE html>
<html lang="en">
<head>
	<title>Interkoneksi</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/favicon.ico')}}" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/vendor/animate/animate.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/vendor/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/vendor/animsition/css/animsition.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/vendor/select2/select2.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/vendor/daterangepicker/daterangepicker.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/login/css/main.css') }}">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100"  style="background-image: url('public/binary.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					LOGIN INTERKONEKSI
                </span>
                <form class="login100-form validate-form p-b-33 p-t-5" role="form" method="post" action="login">
                {{ csrf_field() }}
					<div class="wrap-input100 validate-input" data-validate="Enter Year">
						<input class="input100" type="text" name="year" placeholder="Year" required>
						<span class="focus-input100" data-placeholder="&#xe800;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Username" required>
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password" required>
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>

					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn" type="submit">
							Login
						</button>
                    </div>
                    <br/>
                    @include('layouts.alert')
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="{{ asset('public/login/vendor/jquery/jquery-3.2.1.min') }}'"></script>
<!--===============================================================================================-->
	<script src="{{ asset('public/login/vendor/animsition/js/animsition.min') }}'"></script>
<!--===============================================================================================-->
	<script src="{{ asset('public/login/vendor/bootstrap/js/popper') }}'"></script>
	<script src="{{ asset('public/login/vendor/bootstrap/js/bootstrap.min') }}'"></script>
<!--===============================================================================================-->
	<script src="{{ asset('public/login/vendor/select2/select2.min') }}'"></script>
<!--===============================================================================================-->
	<script src="{{ asset('public/login/vendor/daterangepicker/moment.min') }}'"></script>
	<script src="{{ asset('public/login/vendor/daterangepicker/daterangepicker') }}'"></script>
<!--===============================================================================================-->
	<script src="{{ asset('public/login/vendor/countdowntime/countdowntime') }}'"></script>
<!--===============================================================================================-->
	<script src="{{ asset('public/login/js/main') }}'"></script>

</body>
</html>
