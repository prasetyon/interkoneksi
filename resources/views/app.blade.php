<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/favicon.ico')}}" />

    <title>Dashboard Interkoneksi</title>

    @if(session()->has('role'))
        @include('layouts.css')
    @endif
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @if(session()->has('role'))
            @include('layouts.header')
            @include('layouts.sidebar')
        @endif
        <div class="content-wrapper">
            @if(session()->has('role'))
                @include('layouts.navheader')
            @endif
            <div class="content">
                @yield('content')
            </div>
        </div>

        @if(session()->has('role'))
            @include('layouts.footer')
        @endif
    </div>

    @if(session()->has('role'))
        @include('layouts.js')
    @endif
</body>
</html>
