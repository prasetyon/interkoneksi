@extends('app')
@section('content')

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-sm-12">
            <div class="card card-indigo">
              <div class="card-header">
                <h3 class="card-title">Scheduler Internal List</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" style="table-layout:fixed">
                  <thead>
                    <tr>
                      <th style="text-align: center; vertical-align:middle;">Table</th>
                      <th style="text-align: center; vertical-align:middle;">Jadwal Scheduler</th>
                      <th style="text-align: center; vertical-align:middle;">Last Sync</th>
                      <th style="text-align: center; vertical-align:middle;">Status Scheduler</th>
                      <th style="text-align: center; vertical-align:middle;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data['scheduler'] as $map)
                    <tr>
                      <td>{{$map->name}}</td>
                      <td style="word-wrap: break-word;">{{$map->time}}</td>
                      <td>{{$map->last_update}}</td>
                      <td style="text-align: center; vertical-align:middle;">
                        @php if($map->status=='1') $check = 'checked'; else $check = ''; @endphp
                        <form id="{{'cb'.$map->id}}" action="{{route('switchSchedInternal')}}" method="post">
                          @csrf
                          <input type="text" name="id" value="{{$map->id}}" hidden>
                          <input type="checkbox" name="status" style="width:100%; text-align:center; vertical-align:middle"
                          {{$check}} data-bootstrap-switch data-off-color="danger" data-on-color="success"
                          onChange="document.getElementById('{{'cb'.$map->id}}').submit();">
                        </form>
                      </td>
                      <td style="text-align: center; vertical-align:middle;">
                        <form id="{{'ex'.$map->id}}" action="{{route('executeSchedInternal')}}" method="post">
                          @csrf
                          <input type="text" name="name" value="{{$map->name}}" hidden>
                          <input type="text" name="signature" value="{{$map->signature}}" hidden>
                        </form>
                        <button type="submit" form="{{'ex'.$map->id}}" class="btn btn-md btn-info btn-flat" style="width:auto">Run Manual</button>
                        <button type="button" class="btn btn-md btn-warning btn-flat" style="width:auto" onClick="openModal('{{$map->id}}', '{{$map->name}}', '{{$map->time}}');">Edit</button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->

      <div class="modal fade" id="editsched">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tablesched" class="modal-title">Ubah Waktu Scheduler</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form class="col-lg-12" role="form" id="updatesched" action="{{ route('updateSchedInternal') }}" method="post">
                            {{ csrf_field() }}
                            <input type="text" id="idsched" class="form-control" name="id" required="required" hidden>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idjadwalsched">Jadwal Scheduler -- dipisahkan oleh ; (semicolon)</label>
                                    <input type="text" id="jadwalsched" class="form-control" name="jadwal" placeholder="Jadwal Scheduler"
                                    required="required" autofocus="autofocus">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn btn-primary" type="submit" form="updatesched">Simpan</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.content -->
@endsection

<!-- OPTIONAL SCRIPTS -->
<script>
  function openModal(id, table, jadwal)
  {
    // alert(id+' '+table+' '+jadwal)
    document.getElementById("idsched").value = id;
    $('#tablesched').text("Ubah Jadwal "+table);
    document.getElementById("jadwalsched").value = jadwal;
    $("#editsched").modal('show');
  }
</script>
