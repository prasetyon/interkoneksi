@extends('app')
@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="card card-indigo">
                <div class="card-header">
                    <h3 class="card-title">Scheduler List Krisna - BD Ref Dev</h3>

                    <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped" style="table-layout:fixed">
                        <thead>
                            <tr>
                            <th style="text-align: center; vertical-align:middle;">Table</th>
                            <th style="text-align: center; vertical-align:middle;">Link API</th>
                            <th style="text-align: center; vertical-align:middle;">Jadwal Scheduler</th>
                            <th style="text-align: center; vertical-align:middle;">Last Sync</th>
                            <th style="text-align: center; vertical-align:middle;">Filter</th>
                            <th style="text-align: center; vertical-align:middle;">Status<br/>Scheduler</th>
                            <th style="text-align: center; vertical-align:middle;">Last Update Exist in DB</th>
                            <th style="text-align: center; vertical-align:middle;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data['mapping'] as $map)
                            <tr>
                                <td>{{$map->table}}<br/>{{'('.$map->name.')'}}</td>
                                <td style="word-wrap: break-word;">{{$map->api}}</td>
                                <td style="word-wrap: break-word;">{{$map->scheduler_time}}</td>
                                <td>{{$map->last_update}}<br/>{{$map->status}}</td>
                                <td>
                                    {{  ($map->upsert ? 'Upsert When Approved, ' : 'Upsert All, ').
                                        ($map->delete ? 'Delete When Approved, ' : 'No Delete, ').
                                        ($map->delete_remain ? 'Delete Remain' : 'No Delete Remain') }}
                                </td>
                                <td style="text-align: center; vertical-align:middle;">
                                    @php if($map->scheduler_status=='1') $check = 'checked'; else $check = ''; @endphp
                                    <form id="{{'cb'.$map->id}}" action="{{route('switchSchedStat')}}" method="post">
                                    @csrf
                                    <input type="text" name="id" value="{{$map->id}}" hidden>
                                    <input type="checkbox" name="status" style="width:100%; text-align:center; vertical-align:middle"
                                    {{$check}} data-bootstrap-switch data-off-color="danger" data-on-color="success"
                                    onChange="document.getElementById('{{'cb'.$map->id}}').submit();">
                                    </form>
                                </td>
                                <td>{{$map->updater ? 'Yes' : 'No'}}</td>
                                <td style="text-align: center; vertical-align:middle;">
                                    <form id="{{'ex'.$map->id}}" action="{{route('executeSched')}}" method="post">
                                    @csrf
                                    <input type="text" name="id" value="{{$map->id}}" hidden>
                                    </form>
                                    <button type="submit" form="{{'ex'.$map->id}}" class="btn btn-md btn-info btn-flat" style="width:auto; margin:2px;">Run Manual</button>
                                    <button type="button" class="btn btn-md btn-warning btn-flat" style="width:auto; margin:2px;"
                                        onClick="openModal('{{$map->id}}', '{{$map->table}}', '{{$map->scheduler_time}}',
                                        '{{$map->api}}', '{{$map->upsert}}', '{{$map->delete}}', '{{$map->delete_remain}}', '{{$map->updater}}');">Edit</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        <div class="modal fade" id="editsched">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tablesched" class="modal-title">Ubah Waktu Scheduler</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form class="col-lg-12" role="form" id="updatesched" action="{{ route('updateSched') }}" method="post">
                            {{ csrf_field() }}
                            <input type="text" id="idsched" class="form-control" name="id" required="required" hidden>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idjadwalsched">Jadwal Scheduler -- dipisahkan oleh ; (semicolon)</label>
                                    <input type="text" id="jadwalsched" class="form-control" name="jadwal" placeholder="Jadwal Scheduler"
                                    required="required" autofocus="autofocus">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idapi">URL API</label>
                                    <input type="text" id="api" class="form-control" name="api" placeholder="URL API"
                                    required="required">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <div class="form-label-group">
                                        <label for="idupsert">Upsert</label>
                                        <select class="form-control" style="width:100%" id="upsert" name="upsert" required="required">
                                            <option value="1">Upsert When Approved</option>
                                            <option value="0">Upsert All</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group">
                                        <label for="iddeletez">Delete</label>
                                        <select class="form-control" style="width:100%" id="deletez" name="deletez" required="required">
                                            <option value="1">Delete When Approved</option>
                                            <option value="0">No Delete</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group">
                                        <label for="iddeleter">Delete Remaining Data</label>
                                        <select class="form-control" style="width:100%" id="deleter" name="deleter" required="required">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idkrisnacol">Last Update Exist in DB</label>
                                <select class="form-control" style="width:100%" id="updater" name="updater" required="required">
                                    <option value="1">tglupdate, updater, dan kdupdate ada di tabel seluruh DB</option>
                                    <option value="0">Tidak ada</option>
                                </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn btn-primary" type="submit" form="updatesched">Simpan</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.content -->
@endsection

<!-- OPTIONAL SCRIPTS -->
<script>
    function openModal(id, table, jadwal, api, upsert, deletez, deleter, updater)
    {
        // alert(id+' '+table+' '+jadwal)
        document.getElementById("idsched").value = id;
        $('#tablesched').text("Ubah Jadwal Scheduler "+table);
        document.getElementById("jadwalsched").value = jadwal;
        document.getElementById("api").value = api;
        $('#upsert').val(upsert).change();
        $('#deletez').val(deletez).change();
        $('#deleter').val(deleter).change();
        $('#updater').val(updater).change();
        $("#editsched").modal('show');
    }
</script>
