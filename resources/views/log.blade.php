@extends('app')
@section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-sm-12">
            <div class="card card-indigo">
              <div class="card-header">
                <h3 class="card-title">Log Sync</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" style="table-layout:auto">
                  <thead>
                    <tr>
                      <th style="text-align: center; vertical-align:middle;">NO</th>
                      <th style="text-align: center; vertical-align:middle;">Waktu</th>
                      <th style="text-align: center; vertical-align:middle;">Table</th>
                      <th style="text-align: center; vertical-align:middle;">Source</th>
                      <th style="text-align: center; vertical-align:middle;">Dest</th>
                      <th style="text-align: center; vertical-align:middle;">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $i=1; @endphp
                    @foreach($data['sync'] as $map)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$map->created_at}}</td>
                      <td>{{$map->table}}</td>
                      <td>{{$map->source}}</td>
                      <td>{{$map->dest}}</td>
                      <td>{{$map->status}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection