@extends('app')
@section('content')

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-sm-12">
            <div class="card card-indigo">
              <div class="card-header">
                <h3 class="card-title">Data Mapping</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" style="table-layout:fixed">
                  <thead>
                    <tr>
                        <th style="text-align: center; vertical-align:middle;">Seq</th>
                        <th style="text-align: center; vertical-align:middle;">Table</th>
                        <th style="text-align: center; vertical-align:middle;">Primary Key</th>
                        <th style="text-align: center; vertical-align:middle;">Kolom tampil di Sync</th>
                        <th style="text-align: center; vertical-align:middle;">Kolom dibandingkan di Sync</th>
                        <th style="text-align: center; vertical-align:middle;">Kolom KRISNA</th>
                        <th style="text-align: center; vertical-align:middle;">Last Update Exist in DB</th>
                        <th style="text-align: center; vertical-align:middle;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data['mapping'] as $map)
                    <tr>
                        <td>{{$map->seq}}</td>
                        <td>{{$map->table}}</td>
                        <td style="word-wrap: break-word;">{{$map->primarykey}}</td>
                        <td style="word-wrap: break-word;">{{$map->select}}</td>
                        <td style="word-wrap: break-word;">{{$map->compare}}</td>
                        <td style="word-wrap: break-word;">{{$map->krisna_cols}}</td>
                        <td style="word-wrap: break-word;">{{$map->updater ? 'Yes' : 'No'}}</td>
                        <td style="text-align: center; vertical-align:middle;">
                            <button type="button" class="btn btn-md btn-warning btn-flat" style="width:auto"
                                onClick="openModal('{{str_replace(',', ';',$map->id)}}', '{{str_replace(',', ';',$map->table)}}',
                                    '{{str_replace(',', ';',$map->primarykey)}}', '{{str_replace(',', ';',$map->select)}}',
                                    '{{str_replace(',', ';',$map->compare)}}', '{{str_replace(',', ';',$map->krisna_cols)}}',
                                    '{{str_replace(',', ';',$map->updater)}}');">Edit</button>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->

      <div class="modal fade" id="editsched">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tablesched" class="modal-title">Ubah Mapping</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form class="col-lg-12" role="form" id="updatesched" action="{{ route('updatemapping') }}" method="post">
                            {{ csrf_field() }}
                            <input type="text" id="idsched" class="form-control" name="id" required="required" hidden>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idprimarykey">Primary Key -- dipisahkan oleh , (comma-spasi)</label>
                                    <input type="text" id="primarykey" class="form-control" name="primarykey" placeholder="Primary Key"
                                    required="required" autofocus="autofocus">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idselect">Kolom Ditampilkan -- dipisahkan oleh , (comma-spasi)</label>
                                    <input type="text" id="select" class="form-control" name="select" placeholder="Shown Field"
                                    required="required" autofocus="autofocus">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idcompare">Kolom Dibandingkan -- dipisahkan oleh , (comma-spasi)</label>
                                    <input type="text" id="compare" class="form-control" name="compare" placeholder="Compared Field"
                                    required="required" autofocus="autofocus">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idkrisnacol">Kolom Balikan Krisna -- dipisahkan oleh ,(comma)</label>
                                    <input type="text" id="krisnacol" class="form-control" name="krisnacol" placeholder="Return Cols KRISNA"
                                    required="required" autofocus="autofocus">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label-group">
                                <label for="idkrisnacol">Last Update Exist in DB</label>
                                <select class="form-control" style="width:100%" id="updater" name="updater" required="required">
                                    <option value="1">tglupdate, updater, dan kdupdate ada di tabel seluruh DB</option>
                                    <option value="0">Tidak ada</option>
                                </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn btn-primary" type="submit" form="updatesched">Simpan</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.content -->
@endsection

<!-- OPTIONAL SCRIPTS -->
<script>
  function openModal(id, table, primarykey, select, compare, krisnacol, updater)
  {
    // alert(id+' '+table+' '+jadwal)
    document.getElementById("idsched").value = id;
    $('#tablesched').text("Ubah Jadwal "+table);
    document.getElementById("primarykey").value = primarykey.replaceAll(';', ',');
    document.getElementById("compare").value = compare.replaceAll(';', ',');
    document.getElementById("select").value = select.replaceAll(';', ',');
    document.getElementById("krisnacol").value = krisnacol.replaceAll(';', ',');
    $('#updater').val(updater).change();
    $("#editsched").modal('show');
  }
</script>
