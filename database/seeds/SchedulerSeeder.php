<?php

use Illuminate\Database\Seeder;

class SchedulerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedulers')->insert([
            'table' => 't_dept',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_es2',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_giat',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_giatin',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_giatsas',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_janpres',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_kmpnen',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_kro',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_nawacita',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_output',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_outputin',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_prigiat',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_prinas',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_priprog',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_priproy',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_progout',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_progoutin',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_program',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_program_lintas',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_progsas',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_progsasin',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_sasaran',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_sasaranin',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_soutput',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '01:15;05:15;07:15;09:15;10:15;11:15;12:15;13:15;14:15;15:15;16:15;17:15;19:15;20:15;21:15',
            'status' => '1'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_tema',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_unit',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_visi',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_misi',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('schedulers')->insert([
            'table' => 't_soutputin',
            'source' => 'dbref',
            'dest' => 'dbrefprod',
            'time' => '03:00',
            'status' => '0'
        ]);
    }
}
