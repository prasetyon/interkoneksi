<?php

use Database\Seeders\SchedulerSeqSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SchedulerSeqSeeder::class);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'krisna',
        //     'dest' => 'dbref',
        //     'status' => 'manual',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'krisna',
        //     'dest' => 'dbref',
        //     'status' => 'auto',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'auto',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'auto',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'auto',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'manual',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'manual',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'manual',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'manual',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'manual',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('syncs')->insert([
        //     'table' => 't_giat',
        //     'source' => 'dbref',
        //     'dest' => 'span',
        //     'status' => 'manual',
        //     'created_at' => '2019-01-01 20:00:01',
        //     'updated_at' => '2019-01-01 20:00:01',
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'RKAKL 16',
        //     'db' => 'dbref',
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'RKAKL 60',
        //     'db' => 'dbrefprod',
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'CW',
        //     'db' => 'cwspan',
        // ]);

        // DB::table('users')->insert([
        //     'name' => 'Superadmin',
        //     'username' => 'su',
        //     'role' => 'admin',
        //     'password' => bcrypt('su'),
        // ]);
        // $this->call(InternalSchedulerSeeder::class);
        // $this->call(TableSeeder::class);
    }
}
