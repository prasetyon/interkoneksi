<?php

use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mappings')->insert([
            'name' => 'Departemen',
            'table' => 't_dept',
            'primarykey' => 'kddept',
            'select' => 'kddept, nmdept',
            'compare' => 'nmdept',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getDepartemen",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Eselon 2',
            'table' => 't_es2',
            'primarykey' => 'kddept, kdunit, kdes2',
            'select' => 'kddept, kdunit, kdes2, nmes2',
            'compare' => 'nmes2',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getEs2",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Kegiatan',
            'table' => 't_giat',
            'primarykey' => 'kddept, kdunit, kdprogram, kdes2, kdgiat',
            'select' => 'kddept, kdunit, kdprogram, kdes2, kdgiat, nmgiat',
            'compare' => 'nmgiat',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getGiat",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Indikator Kegiatan',
            'table' => 't_giatin',
            'primarykey' => 'kddept, kdunit, kdprogram, kdes2, kdgiat, kdgiatsas, kdindi',
            'select' => 'kddept, kdunit, kdprogram, kdes2, kdgiat, kdgiatsas, kdindi, nmindi',
            'compare' => 'nmindi, satuan, target_0, target_1, target_2, target_3',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getGiatIn",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Sasaran Kegiatan',
            'table' => 't_giatsas',
            'primarykey' => 'kddept, kdunit, kdprogram, kdes2, kdgiat, kdgiatsas',
            'select' => 'kddept, kdunit, kdprogram, kdes2, kdgiat, kdgiatsas, nmgiatsas, kdsasprog',
            'compare' => 'nmgiatsas, kdsasprog',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getGiatSas",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Janji Presiden',
            'table' => 't_janpres',
            'primarykey' => 'kdjanpres',
            'select' => 'kdjanpres, nmjanpres, gpjanpres',
            'compare' => 'nmjanpres, gpjanpres',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getJanpres",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Komponen',
            'table' => 't_kmpnen',
            'primarykey' => 'kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen',
            'select' => 'kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, nmkmpnen',
            'compare' => 'nmkmpnen, n1, n2, n3, n4, indekskali, indeksout, kdsbiaya, nmindi, satindi',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getKomponen",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Misi',
            'table' => 't_misi',
            'primarykey' => 'kddept, kdmisi',
            'select' => 'kddept, kdmisi, nmmisi',
            'compare' => 'nmmisi',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getMisi",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Nawacita',
            'table' => 't_nawacita',
            'primarykey' => 'kdnawacita',
            'select' => 'kdnawacita, nmnawacita',
            'compare' => 'nmnawacita',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getNawacita",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Output',
            'table' => 't_output',
            'primarykey' => 'kdgiat, kdoutput',
            'select' => 'kdgiat, kdoutput, nmoutput, sat',
            'compare' => 'nmoutput, sat, kdsum, kdjanpres, kdnawacita, kdpn, kdpp, kdkp, kdproy, kdtema',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getOutput",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Indikator Output',
            'table' => 't_outputin',
            'primarykey' => 'thang, kddept, kdunit, kdprogram, kdes2, kdgiat, kdoutput, kdindi',
            'select' => 'thang, kddept, kdunit, kdprogram, kdes2, kdgiat, kdoutput, kdindi, nmindi, satuan',
            'compare' => 'nmindi, satuan, target_0, target_1, target_2, target_3',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getOutputIn",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Prioritas Nasional',
            'table' => 't_prinas',
            'primarykey' => 'kdpn',
            'select' => 'kdpn, nmpn',
            'compare' => 'nmpn',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getPriority",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Prioritas Program',
            'table' => 't_priprog',
            'primarykey' => 'kdpn, kdpp',
            'select' => 'kdpn, kdpp, nmpp',
            'compare' => 'nmpp',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getPriority",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Prioritas Kegiatan',
            'table' => 't_prigiat',
            'primarykey' => 'kdpn, kdpp, kdkp',
            'select' => 'kdpn, kdpp, kdkp, nmkp',
            'compare' => 'nmkp',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getPriority",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Prioritas Proyeksi',
            'table' => 't_priproy',
            'primarykey' => 'kdpn, kdpp, kdkp, kdproy',
            'select' => 'kdpn, kdpp, kdkp, kdproy, nmproy',
            'compare' => 'nmproy',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getPriority",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Output Program',
            'table' => 't_progout',
            'primarykey' => 'kddept, kdunit, kdprogram, kdprogout',
            'select' => 'kddept, kdunit, kdprogram, kdprogout, nmprogout, kdprogsas',
            'compare' => 'nmprogout',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getProgOut",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Indikator Output Program',
            'table' => 't_progoutin',
            'primarykey' => 'kddept, kdunit, kdprogram, kdprogout, kdindi',
            'select' => 'kddept, kdunit, kdprogram, kdprogout, kdindi, nmindi, satuan',
            'compare' => 'nmindi, satuan, target_0, target_1, target_2, target_3',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getProgOutIn",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Program',
            'table' => 't_program',
            'primarykey' => 'kddept, kdunit, kdprogram',
            'select' => 'kddept, kdunit, kdprogram, nmprogram',
            'compare' => 'nmprogram',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getProgram",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Sasaran Program',
            'table' => 't_progsas',
            'primarykey' => 'kddept, kdunit, kdprogram, kdprogsas',
            'select' => 'kddept, kdunit, kdprogram, kdprogsas, nmprogsas, kdsasaran',
            'compare' => 'nmprogsas, kdsasaran',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getProgsas",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Indikator Sasaran Program',
            'table' => 't_progsasin',
            'primarykey' => 'kddept, kdunit, kdprogram, kdprogsas, kdindi',
            'select' => 'kddept, kdunit, kdprogram, kdprogsas, kdindi, nmindi, satuan',
            'compare' => 'nmindi, satuan, target_0, target_1, target_2, target_3',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getProgsasin",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Sasaran',
            'table' => 't_sasaran',
            'primarykey' => 'kddept, kdsasaran',
            'select' => 'kddept, kdsasaran, nmsasaran',
            'compare' => 'nmsasaran',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getSasaran",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Indikator Sasaran',
            'table' => 't_sasaranin',
            'primarykey' => 'kddept, kdsasaran, kdindi',
            'select' => 'kddept, kdsasaran, kdindi, nmindi, satuan',
            'compare' => 'nmindi, satuan, target_0, target_1, target_2, target_3',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "0",
            'scheduler' => "scheduler:getSasaranIn",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Sub Output',
            'table' => 't_soutput',
            'primarykey' => 'kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput',
            'select' => 'kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, nmsoutput',
            'compare' => 'nmsoutput, kdpn, kdpp, kdkp',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getSubOutput",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Indikator Sub-Output',
            'table' => 't_soutputin',
            'primarykey' => 'thang, kddept, kdunit, kdprogram, kdes2, kdgiat, kdoutput, kdsoutput, kdindi',
            'select' => 'thang, kddept, kdunit, kdprogram, kdes2, kdgiat, kdoutput, kdsoutput, kdindi, nmindi, satuan',
            'compare' => 'nmindi, satuan, target_0, target_1, target_2, target_3',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getSoutputIn",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Tema',
            'table' => 't_tema',
            'primarykey' => 'kdtema',
            'select' => 'kdtema, nmtema',
            'compare' => 'nmtema',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getTema",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Unit',
            'table' => 't_unit',
            'primarykey' => 'kddept, kdunit',
            'select' => 'kddept, kdunit, nmunit',
            'compare' => 'nmunit, jabatan1, jabatan2, nip, nama, email',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getUnit",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Visi',
            'table' => 't_visi',
            'primarykey' => 'kddept, kdvisi',
            'select' => 'kddept, kdvisi, nmvisi',
            'compare' => 'nmvisi',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getVisi",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'Program Lintas',
            'table' => 't_program_lintas',
            'primarykey' => 'kdprogram',
            'select' => 'kdprogram, nmprogram',
            'compare' => 'nmprogram',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getProgramLintas",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);

        DB::table('mappings')->insert([
            'name' => 'KRO',
            'table' => 't_kro',
            'primarykey' => 'kdoutput',
            'select' => 'kdoutput, nmoutput, sat',
            'compare' => 'nmoutput',
            'source' => 'krisna',
            'to' => 'cw',
            'updater' => "1",
            'scheduler' => "scheduler:getKRO",
            'scheduler_time' => "01:00",
            'scheduler_status' => 1,
            'category' => 'KRISNA',
        ]);
    }
}
