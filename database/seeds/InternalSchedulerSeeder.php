<?php

use Illuminate\Database\Seeder;

class InternalSchedulerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('internal_schedulers')->insert([
            'name' => 'Scheduler Cadangan',
            'year' => 2021,
            'detail' => 'Scheduler create output, soutput, dan komponen cadangan u/ setiap kdgiat',
            'signature' => 'internalscheduler:cadangan',
            'time' => '03:00',
            'status' => '0'
        ]);
        DB::table('internal_schedulers')->insert([
            'name' => 'Scheduler Sat1',
            'year' => 2021,
            'detail' => 'Scheduler untuk menambahkan sat1 di soutput untuk setiap soutputin nya',
            'signature' => 'internalscheduler:sat1',
            'time' => '03:00',
            'status' => '0'
        ]);
    }
}
