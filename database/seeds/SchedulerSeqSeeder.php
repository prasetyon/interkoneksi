<?php

namespace Database\Seeders;

use App\Mapping;
use Illuminate\Database\Seeder;

class SchedulerSeqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mapping::where('table', 't_dept')->update(['seq' => 1]);
        Mapping::where('table', 't_unit')->update(['seq' => 2]);
        Mapping::where('table', 't_program')->update(['seq' => 3]);
        Mapping::where('table', 't_progout')->update(['seq' => 4]);
        Mapping::where('table', 't_progoutin')->update(['seq' => 5]);
        Mapping::where('table', 't_progsas')->update(['seq' => 6]);
        Mapping::where('table', 't_progsasin')->update(['seq' => 7]);

        Mapping::where('table', 't_giat')->update(['seq' => 8]);
        Mapping::where('table', 't_giatin')->update(['seq' => 9]);
        Mapping::where('table', 't_giatsas')->update(['seq' => 10]);
        Mapping::where('table', 't_output')->update(['seq' => 11]);
        Mapping::where('table', 't_outputin')->update(['seq' => 12]);
        Mapping::where('table', 't_soutput')->update(['seq' => 13]);
        Mapping::where('table', 't_soutputin')->update(['seq' => 14]);
        Mapping::where('table', 't_kmpnen')->update(['seq' => 15]);
    }
}
