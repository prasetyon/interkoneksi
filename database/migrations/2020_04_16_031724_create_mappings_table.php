<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mappings', function (Blueprint $table) {
            $table->id();
            $table->string('table');
            $table->string('name');
            $table->string('primarykey');
            $table->string('select');
            $table->string('compare');
            $table->string('source');
            $table->string('to');
            $table->string('updater', 1);
            $table->string('scheduler')->nullable();
            $table->string('scheduler_time')->nullable();
            $table->string('scheduler_status',1)->default('0');
            $table->string('category')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mappings');
    }
}
