<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', ['as' => 'login', 'uses' => 'AuthController@index']);
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);


Route::middleware('admin')->group(function () {
    Route::get('/', ['as' => '/', 'uses' => 'SyncController@index']);
    Route::post('/', ['as' => '/', 'uses' => 'SyncController@index']);
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'SyncController@index']);
    Route::post('dashboard', ['as' => 'dashboard', 'uses' => 'SyncController@index']);

    Route::get('scheduler', ['as' => 'scheduler', 'uses' => 'SchedulerController@index']);
    Route::post('updateSched', ['as' => 'updateSched', 'uses' => 'SchedulerController@update']);
    Route::post('switchSchedStat', ['as' => 'switchSchedStat', 'uses' => 'SchedulerController@switchStatus']);

    Route::get('scheduler2', ['as' => 'scheduler2', 'uses' => 'SchedulerController@index2']);
    Route::post('updateSched2', ['as' => 'updateSched2', 'uses' => 'SchedulerController@update2']);
    Route::post('switchSchedStat2', ['as' => 'switchSchedStat2', 'uses' => 'SchedulerController@switchStatus2']);

    Route::post('executeSched', ['as' => 'executeSched', 'uses' => 'SchedulerController@execute']);

    Route::post('multisync', ['as' => 'multisync', 'uses' => 'SyncController@multiTableSync']);
    Route::post('multirowsynca', ['as' => 'multirowsynca', 'uses' => 'SyncController@multiRowToDB1']);
    Route::post('multirowsyncb', ['as' => 'multirowsyncb', 'uses' => 'SyncController@multiRowToDB2']);
    Route::post('multinomsync', ['as' => 'multinomsync', 'uses' => 'SyncController@multiNomSync']);

    Route::get('schedinter', ['as' => 'schedinter', 'uses' => 'SchedulerInternalController@index']);
    Route::post('updateSchedInternal', ['as' => 'updateSchedInternal', 'uses' => 'SchedulerInternalController@update']);
    Route::post('switchSchedInternal', ['as' => 'switchSchedInternal', 'uses' => 'SchedulerInternalController@switchStatus']);
    Route::post('executeSchedInternal', ['as' => 'executeSchedInternal', 'uses' => 'SchedulerInternalController@execute']);

    Route::get('mapping', ['as' => 'mapping', 'uses' => 'MappingController@index']);
    Route::post('updatemapping', ['as' => 'updatemapping', 'uses' => 'MappingController@update']);

    Route::get('log', ['as' => 'log', 'uses' => 'SyncController@log']);
});
