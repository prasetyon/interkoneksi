<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('revbun', ['as' => 'revbun', 'uses' => 'BunController@rev']);
Route::get('soutputrevbun', ['as' => 'soutputrevbun', 'uses' => 'BunController@soutput']);

Route::get('lokasi', ['as' => 'lokasi', 'uses' => 'KrisnaController@lokasi']);
Route::get('kabkota', ['as' => 'kabkota', 'uses' => 'KrisnaController@kabkota']);
Route::get('krisna', ['as' => 'krisna', 'uses' => 'KrisnaController@index']);
Route::get('volsout', ['as' => 'volsout', 'uses' => 'KrisnaController@volsoutput']);
Route::get('pagu', ['as' => 'pagu', 'uses' => 'KrisnaController@pagu']);
Route::get('kpjm', ['as' => 'kpjm', 'uses' => 'KrisnaController@kpjm']);
Route::get('volume', ['as' => 'volume', 'uses' => 'KrisnaController@volume']);
Route::get('realisasi', ['as' => 'realisasi', 'uses' => 'KrisnaController@realisasi']);
Route::get('tableDiff', ['as' => 'tableDiff', 'uses' => 'SyncController@show']);

Route::prefix('kominfo')->group(function () {
    Route::get('ega', ['as' => 'ega', 'uses' => 'KominfoController@ega']);
});