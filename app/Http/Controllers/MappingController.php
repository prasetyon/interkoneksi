<?php

namespace App\Http\Controllers;

use App\Mapping;
use Illuminate\Http\Request;

class MappingController extends Controller
{
    //
    public function index(Request $request)
    {
        $year = $request->session()->get('year');

        $data['mapping'] = Mapping::where('year', $year)->orderBy('seq')->orderBy('table')->get();

        return view('mapping')->with('data', $data)->with('menu', 'mapping');
    }

    public function update(Request $request)
    {
        $year = $request->session()->get('year');
        // dd($request);
        $update = Mapping::where('id', $request->id)
            ->update([
                'primarykey' => $request->primarykey,
                'select' => $request->select,
                'compare' => $request->compare,
                'krisna_cols' => $request->krisnacol,
                'updater' => $request->updater
            ]);

        return redirect()->route('mapping');
    }
}
