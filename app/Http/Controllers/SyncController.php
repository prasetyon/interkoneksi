<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Mapping;
use App\Category;
use App\Sync;
// use DB;
use Datetime;
use Illuminate\Support\Facades\Log;

class SyncController extends Controller
{
    //
    public function log(Request $request)
    {
        $year = $request->session()->get('year');
        $data['sync'] = Sync::where('year', $year)->orderBy('created_at', 'desc')->offset(0)->limit(200)->get();

        // foreach($sync as $s)
        // {
        //     echo $s->created_at.': '.$s->status."\t".' sync '.$s->table.' from '.$s->source.' to '.$s->dest.'<br/>';
        // }
        return view('log')->with('data', $data)->with('menu', 'log');
    }

    public function index(Request $request)
    {
        $year = $request->session()->get('year');
        $data['selected1'] = isset($request->cat1) ? $request->cat1 : 'dbref';
        $data['selected2'] = isset($request->cat2) ? $request->cat2 : 'dbrefprod';
        $data['mapping'] = isset($request->category) ? Mapping::where('year', $year)->where('category', 'LIKE', '%' . $request->category . '%')->orderBy('table', 'asc')->get() : Mapping::where('year', $year)->orderBy('table', 'asc')->get();
        $data['category'] = Category::all();

        $i = 0;
        $datas = null;

        $this->setCurrentYearConnection($year, $data['selected1'], $data['selected2']);

        foreach ($data['mapping'] as $map) {

            $dbref = DB::connection($data['selected1'])->table($map->table);
            $dbrev = DB::connection($data['selected2'])->table($map->table);

            $zzcount1 = $dbref->get()->count();

            $zzcount2 = Schema::connection($data['selected2'])->hasTable($map->table) ? $dbrev->get()->count() : 0;

            if ($zzcount1 > 0 && $zzcount2 > 0) {
                $datas[$i] = new \stdClass();
                $datas[$i]->count1 = $zzcount1;
                $datas[$i]->count2 = $zzcount2;
                $datas[$i]->table = $map->table . ' (' . $map->name . ')';
                $datas[$i]->tbl = $map->table;

                if ($map->updater == "1") {
                    $upref = $dbref->select('tglupdate')->orderBy('tglupdate', 'desc')->first();
                    $uprev = $dbrev->select('tglupdate')->orderBy('tglupdate', 'desc')->first();

                    $diff = strtotime(substr($upref->tglupdate, 0, 10)) - strtotime(substr($uprev->tglupdate, 0, 10));
                    $years = floor($diff / (365 * 60 * 60 * 24));
                    $months = floor($diff / (30 * 60 * 60 * 24));
                    $days = floor($diff / (60 * 60 * 24));

                    $datas[$i]->updated1 = $upref->tglupdate;
                    $datas[$i]->updated2 = $uprev->tglupdate;
                    $datas[$i]->interval = $days;
                } else {
                    $datas[$i]->updated1 = "-";
                    $datas[$i]->updated2 = "-";
                    $datas[$i]->interval = 0;
                }

                $i++;
            }
        }

        $data['data'] = $datas;

        if (isset($request->table) && $request->table != 'summary') {
            // echo $request->table;
            $source = Mapping::where('year', $year)->where('table', $request->table)->first();
            $key = $source->primarykey;
            $comp = $source->compare;
            $select = $source->select;
            $nom = $source->nomenklatur;

            $dbref = DB::connection($data['selected1'])->table($request->table)->count();
            $dbrev = DB::connection($data['selected2'])->table($request->table)->count();

            $res[0] = new \stdClass();
            $res[0]->name = 'Data Count';
            $res[0]->t1 = $dbref;
            $res[0]->t2 = $dbrev;
            $stat = "";
            if ($res[0]->t1 > $res[0]->t2) $stat .= "t2|danger|down|" . ($res[0]->t1 - $res[0]->t2);
            else if ($res[0]->t1 < $res[0]->t2) $stat .= "t2|danger|up|" . ($res[0]->t2 - $res[0]->t1);
            else $stat .= "t2|success|right|OK";
            $res[0]->ket = $stat;

            $dbrefzz = DB::connection($data['selected1'])
                ->table($request->table)
                ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
                ->orderByRaw('col')
                ->get();
            $i = 0;
            foreach ($dbrefzz as $dd) {
                $dbrefz[$i]['key'] = $dd->col;
                $dbrefz[$i++]['comp'] = $dd->comp;
            }
            $cwspanzz = DB::connection($data['selected2'])
                ->table($request->table)
                ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
                ->orderByRaw('col')
                ->get();
            $i = 0;
            foreach ($cwspanzz as $cc) {
                $cwspanz[$i]['key'] = $cc->col;
                $cwspanz[$i++]['comp'] = $cc->comp;
            }

            // dd($cwspanz);
            $diffz = $this->findDiff($cwspanz, $dbrefz, $key, $comp, $data['selected1'], $data['selected2']);

            // dd($diffz);

            $nomdiff = $diffz['comp'];
            $coldiff = $diffz['col'];
            // dd($coldiff);

            $diff1 = DB::connection($data['selected1'])->table($request->table)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->selectRaw("CONCAT_ws('|'," . $key . ") as keyz, " . $select)->get();
            $diff2 = DB::connection($data['selected2'])->table($request->table)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->selectRaw("CONCAT_ws('|'," . $key . ") as keyz, " . $select)->get();

            $res[1] = new \stdClass();
            $res[1]->name = 'Data Count Diff';
            $res[1]->id = 'datadiff';
            $res[1]->t1 = count($diff1) . ' data tidak terdapat di ' . $data['selected2'];
            $res[1]->t2 = count($diff2) . ' data tidak terdapat di ' . $data['selected1'];
            $res[1]->t1data = $diff1;
            $res[1]->t2data = $diff2;
            $res[1]->t1diff = 'multirowsyncb';
            $res[1]->t2diff = 'multirowsynca';
            $res[1]->cb1 = 'cbr1';
            $res[1]->cb2 = 'cbr2';
            $res[1]->column = str_replace(" ", "", explode(',', $select));
            $res[1]->ket = "-";

            $nomz = new \stdClass();
            $nomz->name = 'Data Diff';
            $nomz->id = 'nomdiff';
            $nomz->t1 = "";
            $nomz->t2 = count($nomdiff) . ' data berbeda';
            $nomz->t2data = $nomdiff;
            $nomz->t2diff = 'multinomsync';
            $nomz->cb2 = 'cbn2';
            $nomz->column = str_replace(" ", "", explode(',', $key . "," . $data['selected1'] . "," . $data['selected2'] . ",diff"));
            $nomz->ket = (count($nomdiff) > 0) ? 't2|danger|down|' : "t2|success|right|OK";

            // dd($nomz);
            $data['res'] = $res;
            return view('dashboard')->with('data', $data)->with('nom', $nomz)->with('state', $request->table)->with('menu', 'sync');
        }

        return view('dashboard')->with('data', $data)->with('state', 'summary')->with('menu', 'sync');
    }

    public function multiTableSync(Request $request)
    {
        $year = $request->session()->get('year');
        if (strlen($request->tablesync) < 1) redirect()->back();
        $sync = explode('|', substr($request->tablesync, 0, strlen($request->tablesync) - 1));
        $db1 = $request->statetablesync == 0 ? $request->db1ts : $request->db2ts;
        $db2 = $request->statetablesync == 0 ? $request->db2ts : $request->db1ts;

        $this->setCurrentYearConnection($year, $db1, $db2);
        // echo 'sync from ' . $db1 . ' to ' . $db2 . '<br/>';
        // dd($sync);

        foreach ($sync as $s) {
            // echo 'table ' . $s . '<br/>';
            $this->setCurrentYearConnection($year, $db1, $db2);
            Log::info('Running scheduler get ' . $s . ' from ' . $db1 . ' to ' . $db2);

            $source = Mapping::where('year', $year)->where('table', $s)->first();
            $key = $source->primarykey;
            $select = $source->select;
            $comp = $source->compare;
            $keyex = explode(', ', $key);
            $compex = explode(', ', $comp);

            $dbrefz = $cwspanz = [];

            $dbrefzz = DB::connection($db1)
                ->table($s)
                ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
                ->orderByRaw('col')
                ->get();
            $i = 0;
            foreach ($dbrefzz as $dd) {
                $dbrefz[$i]['key'] = $dd->col;
                $dbrefz[$i++]['comp'] = $dd->comp;
            }
            $cwspanzz = DB::connection($db2)
                ->table($s)
                ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
                ->orderByRaw('col')
                ->get();
            $i = 0;
            foreach ($cwspanzz as $cc) {
                $cwspanz[$i]['key'] = $cc->col;
                $cwspanz[$i++]['comp'] = $cc->comp;
            }
            $diffz = $this->findDiff($cwspanz, $dbrefz, $key, $comp, $db1, $db2);
            // dd($diffz);

            $nomdiff = $diffz['comp'];
            $coldiff = $diffz['col'];

            $diff1 = DB::connection($db1)->table($s)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->get();
            $diff2 = DB::connection($db2)->table($s)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->delete();

            // INSERT DATA DB1 TO DB2
            foreach ($diff1 as $d) {
                $colname = Schema::connection($db1)->getColumnlisting($s);

                $i = 0;
                $qm = $cm = "";
                foreach ($colname as $colz) {
                    $qm .= "?,";
                    $cm .= ($colz . ",");
                    $tval[$i++] = $d->$colz;
                }
                $qm = substr($qm, 0, strlen($qm) - 1);
                $cm = substr($cm, 0, strlen($cm) - 1);

                $qstate = "insert into " . $s . " (" . $cm . ") values (" . $qm . ")";
                DB::connection($db2)->insert($qstate, $tval);
            }

            foreach ($nomdiff as $n) {
                $wer = array();
                $werzz = array();
                $ii = $iix = 0;
                foreach ($keyex as $k) {
                    $wer[$ii++] = [$k, '=', $n->$k];
                }

                $ixix = 0;
                foreach ($n->diff as $cx) {
                    $werzz[$cx] = $n->$db1[$ixix];
                    $ixix++;
                }

                // dd($werzz);

                $update = DB::connection($db2)
                    ->table($s)
                    ->where($wer)
                    ->update($werzz);
            }

            // echo "<pre>"; print_r(DB::connection($db2)->getQueryLog()); echo "</pre>";
            // dd(0);
            $sync = new Sync();
            $sync->table = $s;
            $sync->source = $db1;
            $sync->dest = $db2;
            $sync->year = $year;
            $sync->status = 'manual';
            $sync->save();

            $sourcezz = DB::connection($db1)->table($s)->get()->count();
            $destzz = DB::connection($db2)->table($s)->get()->count();

            Log::info('Finished running scheduler get ' . $s . ' from ' . $db1 . ' to ' . $db2);
            Log::info('DB TEST: ' . $sourcezz . ' DB PROD ' . $destzz);
        }

        return redirect()->back();
    }

    public function multiRowToDB2(Request $request)
    {
        // dd($request);

        $year = $request->session()->get('year');
        $source = Mapping::where('year', $year)->where('table', $request->table)->first();
        $key = $source->primarykey;

        $this->setCurrentYearConnection($year, $request->db1, $request->db2);

        // INSERT DATA DB1 TO DB2
        $xx = 0;
        foreach ($request->cbr1 as $d) {
            if ($d == "1") {
                $colname = Schema::connection($request->db1)->getColumnlisting($request->table);
                $dataz = DB::connection($request->db1)->table($request->table)->where(DB::raw("CONCAT_ws('|'," . $key . ")"), $request->checked[$xx])->first();

                $i = 0;
                $qm = $cm = "";
                foreach ($colname as $colz) {
                    $qm .= "?,";
                    $cm .= ($colz . ",");
                    $tval[$i++] = $dataz->$colz;
                }
                $qm = substr($qm, 0, strlen($qm) - 1);
                $cm = substr($cm, 0, strlen($cm) - 1);

                $qstate = "insert into " . $request->table . " (" . $cm . ") values (" . $qm . ")";

                // dd($tval);
                // DB::connection($request->db2)->enableQueryLog();
                DB::connection($request->db2)->insert($qstate, $tval);
                // dd(DB::connection($request->db2)->getQueryLog());
            }

            $xx++;
        }

        return redirect()->back();
    }

    public function multiRowToDB1(Request $request)
    {
        // dd($request);

        $year = $request->session()->get('year');
        $source = Mapping::where('year', $year)->where('table', $request->table)->first();
        $key = $source->primarykey;
        $this->setCurrentYearConnection($year, $request->db1, $request->db2);
        // INSERT DATA DB1 TO DB2
        $xx = 0;
        foreach ($request->cbr2 as $d) {
            if ($d == "1") {
                $colname = Schema::connection($request->db1)->getColumnlisting($request->table);
                $dataz = DB::connection($request->db1)->table($request->table)->where(DB::raw("CONCAT_ws('|'," . $key . ")"), $request->checked[$xx])->first();

                $i = 0;
                $qm = $cm = "";
                foreach ($colname as $colz) {
                    $qm .= "?,";
                    $cm .= ($colz . ",");
                    $tval[$i++] = $dataz->$colz;
                }
                $qm = substr($qm, 0, strlen($qm) - 1);
                $cm = substr($cm, 0, strlen($cm) - 1);

                $qstate = "insert into " . $request->table . " (" . $cm . ") values (" . $qm . ")";

                // dd($tval);
                // DB::connection($request->db2)->enableQueryLog();
                DB::connection($request->db2)->insert($qstate, $tval);
                // dd(DB::connection($request->db2)->getQueryLog());
            }

            $xx++;
        }

        return redirect()->back();
    }

    public function multiNomSync(Request $request)
    {
        // dd($request);
        $year = $request->session()->get('year');
        $db1 = $request->statenomsync == 0 ? $request->db1 : $request->db2;
        $db2 = $request->statenomsync == 0 ? $request->db2 : $request->db1;

        $this->setCurrentYearConnection($year, $db1, $db2);
        $source = Mapping::where('year', $year)->where('table', $request->table)->first();
        $comp = $source->compare;
        $key = $source->primarykey;
        $keyex = explode(', ', $key);
        $compex = explode(', ', $comp);

        $xx = 0;
        foreach ($request->cbn2 as $d) {
            if ($d == "1") {
                $colname = Schema::connection($db1)->getColumnlisting($request->table);
                $dataz = DB::connection($db1)->table($request->table)->where(DB::raw("CONCAT_ws('|'," . $key . ")"), $request->checked[$xx])->first();

                $wer = array();
                $werzz = array();
                $ii = 0;
                foreach ($keyex as $k) {
                    $wer[$ii++] = [$k, '=', $dataz->$k];
                }
                $ixix = 0;
                foreach ($compex as $cx) {
                    $werzz[$cx] = $dataz->$cx;
                    $ixix++;
                }

                // dd($werzz);

                $update = DB::connection($db2)
                    ->table($request->table)
                    ->where($wer)
                    ->update($werzz);
            }

            $xx++;
        }

        return redirect()->back();
    }

    private function setCurrentYearConnection($year, $db1, $db2)
    {
        $data['category'] = Category::all();
        foreach ($data['category'] as $cat) {
            if ($cat->db == $db1 || $cat->db == $db2) {
                DB::disconnect($cat->db);
                Config::set('database.connections.' . $cat->db . '.database',  $cat->schema . $year);
            }
        }
    }

    private function findDiff($arr1, $arr2, $key, $comp, $db1, $db2)
    {
        // if(count($arr1)<1) return $arr2;
        // if(count($arr2)<1) return $arr1;

        $res['col'] = [];
        $res['comp'] = [];
        $i = $ii = $j = $x = 0;
        $compex = explode(', ', $comp);

        while ($i < count($arr1) && $j < count($arr2)) {
            if ($arr1[$i]['key'] < $arr2[$j]['key']) {
                $res['col'][$x++] = $arr1[$i]['key'];
                $i++;
            } else if ($arr1[$i]['key'] > $arr2[$j]['key']) {
                $res['col'][$x++] = $arr2[$j]['key'];
                $j++;
            } else {
                if (strcmp($arr1[$i]['comp'], $arr2[$j]['comp'])) {
                    // echo '<br/>compare<br/>' . $arr1[$i]['comp'] . ' with<br/>' . $arr2[$j]['comp'] . '<br/>';

                    $res['comp'][$ii] = new \stdClass();
                    $kk = explode(', ', $key);
                    $idval = explode('|', $arr1[$i]['key']);
                    $zzz = 0;
                    foreach ($kk as $kkk) {
                        $res['comp'][$ii]->{$kkk} = $idval[$zzz++];
                    }

                    // disini kita assign perbedaannya dimana aja dan apa aja
                    $exp1 = explode('|', $arr1[$i]['comp']);
                    $exp2 = explode('|', $arr2[$j]['comp']);

                    $ccind = 0;
                    $cczz = 0;
                    $diffarr = [];
                    foreach ($compex as $ccc) {
                        if (!isset($exp1[$ccind])) $exp1[$ccind] = null;
                        if (!isset($exp2[$ccind])) $exp2[$ccind] = null;

                        // echo '-----compare<br/>' . $exp1[$ccind] . ' with<br/>' . $exp2[$ccind] . '<br/>';
                        if (strcmp($exp1[$ccind], $exp2[$ccind])) {
                            // echo '--------dif<br/>';
                            $diffarr[0][$cczz] = $exp1[$ccind];
                            $diffarr[1][$cczz] = $exp2[$ccind];
                            $diffarr[2][$cczz++] = $ccc;
                        }
                        $ccind++;
                    }

                    $res['comp'][$ii]->{$db2} = $diffarr[0];
                    $res['comp'][$ii]->{$db1} = $diffarr[1];
                    $res['comp'][$ii]->diff = $diffarr[2];

                    $res['comp'][$ii++]->keyz = $arr1[$i]['key'];
                }
                $i++;
                $j++;
            }

            // if ($ii == 50)
            //     dd($res);
        }

        // dd($res);

        while ($i < count($arr1)) {
            $res['col'][$x++] = $arr1[$i];
            $i++;
        }
        while ($j < count($arr2)) {
            $res['col'][$x++] = $arr2[$j];
            $j++;
        }

        return $res;
    }
}
