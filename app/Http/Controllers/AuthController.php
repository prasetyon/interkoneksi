<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Mapping;

class AuthController extends Controller
{
    //
    public function index(Request $request)
    {
        // dd($request);
        if (!$request->session()->has('name')) {
            return view('login');
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $pwd = $request->password;
        $year = $request->year;

        $checkYear = Mapping::select('year')->where('year', $year)->first();
        $auth = User::select('name', 'username', 'password', 'role', 'id')->where('username', '=', $username)->first();
        // dd($auth);

        if ($checkYear && $auth && Hash::check($pwd, $auth->password)) {
            $name = $auth->name;
            $role = $auth->role;
            $id = $auth->id;
            $username = $auth->username;

            $request->session()->put('name', $name);
            $request->session()->put('username', $username);
            $request->session()->put('role', $role);
            $request->session()->put('user_id', $id);
            $request->session()->put('year', $year);
            session()->save();

            // dd($request);

            return redirect()->route('dashboard');
        } else if (!$checkYear) {
            return redirect()->back()->with('alert-danger', 'Tahun tidak tersedia');
        } else if (!isset($auth->name)) {
            return redirect()->back()->with('alert-danger', 'Akun anda tidak ditemukan!');
        } else if (!Hash::check($pwd, $auth->password)) {
            return redirect()->back()->with('alert-danger', 'Password Salah!');
        } else {
            return redirect()->back()->with('alert-danger', 'Anda tidak memiliki kewenangan');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->route('login');
    }
}
