<?php

namespace App\Http\Controllers\api;

use App\Category;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Mapping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SyncController extends Controller
{
    public function show(Request $request)
    {
        if (!$request->filled('table') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $year = $request->year;
        $data['selected1'] = isset($request->cat1) ? $request->cat1 : 'dbref';
        $data['selected2'] = isset($request->cat2) ? $request->cat2 : 'dbrefprod';

        $i = 0;
        $datas = null;

        $this->setCurrentYearConnection($year, $data['selected1'], $data['selected2']);

        $map = Helper::getMappingInfo($request->table, $request->year);
        if ($map == null) {
            return response()->json(['success' => false, 'result' => 'Mapping table not found'], 422);
        }

        if ($map->updater == "1") {
            $upref = DB::connection($data['selected1'])->table($request->table)->select('tglupdate')->orderBy('tglupdate', 'desc')->first();
            $uprev = DB::connection($data['selected2'])->table($request->table)->select('tglupdate')->orderBy('tglupdate', 'desc')->first();

            $diff = strtotime(substr($upref->tglupdate, 0, 10)) - strtotime(substr($uprev->tglupdate, 0, 10));
            $days = floor($diff / (60 * 60 * 24));

            $updated1 = $upref->tglupdate;
            $updated2 = $uprev->tglupdate;
            $interval = $days;
        } else {
            $updated1 = "-";
            $updated2 = "-";
            $interval = 0;
        }

        // echo $request->table;
        $source = Mapping::where('year', $year)->where('table', $request->table)->first();
        $key = $source->primarykey;
        $comp = $source->compare;
        $select = $source->select;
        $nom = $source->nomenklatur;

        $dbref = DB::connection($data['selected1'])->table($request->table)->count();
        $dbrev = DB::connection($data['selected2'])->table($request->table)->count();

        $res = new \stdClass();
        $res->db1 = 'Krisna';
        $res->db2 = 'RKAKL 60';
        $res->count1 = $dbref;
        $res->count2 = $dbrev;
        $res->countdif = abs($dbrev - $dbref);
        $res->updated1 = $updated1;
        $res->updated2 = $updated2;
        $res->interval = $interval;

        $dbrefzz = DB::connection($data['selected1'])
            ->table($request->table)
            ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
            ->orderByRaw('col')
            ->get();
        $i = 0;
        foreach ($dbrefzz as $dd) {
            $dbrefz[$i]['key'] = $dd->col;
            $dbrefz[$i++]['comp'] = $dd->comp;
        }
        $cwspanzz = DB::connection($data['selected2'])
            ->table($request->table)
            ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
            ->orderByRaw('col')
            ->get();
        $i = 0;
        foreach ($cwspanzz as $cc) {
            $cwspanz[$i]['key'] = $cc->col;
            $cwspanz[$i++]['comp'] = $cc->comp;
        }

        // dd($cwspanz);
        $diffz = $this->findDiff($cwspanz, $dbrefz, $key, $comp, $data['selected1'], $data['selected2']);

        // dd($diffz);

        $nomdiff = $diffz['comp'];
        $coldiff = $diffz['col'];

        foreach ($nomdiff as $nn) {

            $i = 0;
            foreach ($nn->diff as $nd) {
                $nomnom = new \stdClass();
                $nomnom->col = $nd;
                $nomnom->db1 = $nn->{$data['selected1']}[$i];
                $nomnom->db2 = $nn->{$data['selected2']}[$i];
                $i++;
                $nn->data[] = $nomnom;
            }

            unset($nn->{$data['selected1']});
            unset($nn->{$data['selected2']});
            unset($nn->diff);
            unset($nn->keyz);
        }

        $diff1 = DB::connection($data['selected1'])->table($request->table)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->selectRaw("CONCAT_ws('|'," . $key . ") as pk, " . $select)->get();
        $diff2 = DB::connection($data['selected2'])->table($request->table)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->selectRaw("CONCAT_ws('|'," . $key . ") as pk, " . $select)->get();

        $countdif = new \stdClass();
        $countdif->info = 'Exist only in a DB';
        $countdif->count1 = count($diff1);
        $countdif->count2 = count($diff2);
        $countdif->data1 = $diff1;
        $countdif->data2 = $diff2;
        // $countdif->t1diff = 'multirowsyncb';
        // $countdif->t2diff = 'multirowsynca';
        // $countdif->cb1 = 'cbr1';
        // $countdif->cb2 = 'cbr2';
        // $countdif->column = str_replace(" ", "", explode(',', $select));
        // $countdif->ket = "-";
        $res->existdif = $countdif;

        $datadif = new \stdClass();
        $datadif->info = 'Exist in both DB but with different data';
        $datadif->count = count($nomdiff);
        $datadif->data = $nomdiff;
        // $datadif->cb2 = 'cbn2';
        // $datadif->column = str_replace(" ", "", explode(',', $key . "," . $data['selected1'] . "," . $data['selected2'] . ",diff"));
        // $datadif->ket = (count($nomdiff) > 0) ? 't2|danger|down|' : "t2|success|right|OK";
        $res->datadif = $datadif;

        return response()->json(['success' => true, 'result' => 'Successfully Get Data Diff', 'data' => $res], 200);
        dd($res);
    }

    private function findDiff($arr1, $arr2, $key, $comp, $db1, $db2)
    {
        // if(count($arr1)<1) return $arr2;
        // if(count($arr2)<1) return $arr1;

        $res['col'] = [];
        $res['comp'] = [];
        $i = $ii = $j = $x = 0;
        $compex = explode(', ', $comp);

        while ($i < count($arr1) && $j < count($arr2)) {
            if ($arr1[$i]['key'] < $arr2[$j]['key']) {
                $res['col'][$x++] = $arr1[$i]['key'];
                $i++;
            } else if ($arr1[$i]['key'] > $arr2[$j]['key']) {
                $res['col'][$x++] = $arr2[$j]['key'];
                $j++;
            } else {
                if (strcmp($arr1[$i]['comp'], $arr2[$j]['comp'])) {
                    // echo '<br/>compare<br/>' . $arr1[$i]['comp'] . ' with<br/>' . $arr2[$j]['comp'] . '<br/>';

                    $res['comp'][$ii] = new \stdClass();
                    $kk = explode(', ', $key);
                    $idval = explode('|', $arr1[$i]['key']);
                    $zzz = 0;
                    foreach ($kk as $kkk) {
                        $res['comp'][$ii]->{$kkk} = $idval[$zzz++];
                    }

                    // disini kita assign perbedaannya dimana aja dan apa aja
                    $exp1 = explode('|', $arr1[$i]['comp']);
                    $exp2 = explode('|', $arr2[$j]['comp']);

                    $ccind = 0;
                    $cczz = 0;
                    $diffarr = [];
                    foreach ($compex as $ccc) {
                        if (!isset($exp1[$ccind])) $exp1[$ccind] = null;
                        if (!isset($exp2[$ccind])) $exp2[$ccind] = null;

                        // echo '-----compare<br/>' . $exp1[$ccind] . ' with<br/>' . $exp2[$ccind] . '<br/>';
                        if (strcmp($exp1[$ccind], $exp2[$ccind])) {
                            // echo '--------dif<br/>';
                            $diffarr[0][$cczz] = $exp1[$ccind];
                            $diffarr[1][$cczz] = $exp2[$ccind];
                            $diffarr[2][$cczz++] = $ccc;
                        }
                        $ccind++;
                    }

                    $res['comp'][$ii]->{$db2} = $diffarr[0];
                    $res['comp'][$ii]->{$db1} = $diffarr[1];
                    $res['comp'][$ii]->diff = $diffarr[2];

                    $res['comp'][$ii++]->keyz = $arr1[$i]['key'];
                }
                $i++;
                $j++;
            }

            // if ($ii == 50)
            //     dd($res);
        }

        // dd($res);

        while ($i < count($arr1)) {
            $res['col'][$x++] = $arr1[$i];
            $i++;
        }
        while ($j < count($arr2)) {
            $res['col'][$x++] = $arr2[$j];
            $j++;
        }

        return $res;
    }

    private function setCurrentYearConnection($year, $db1, $db2)
    {
        $data['category'] = Category::all();
        foreach ($data['category'] as $cat) {
            if ($cat->db == $db1 || $cat->db == $db2) {
                DB::disconnect($cat->db);
                Config::set('database.connections.' . $cat->db . '.database',  $cat->schema . $year);
            }
        }
    }
}
