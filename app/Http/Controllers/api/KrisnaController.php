<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Mapping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use stdClass;

class KrisnaController extends Controller
{
    private $key = 'cbf63220837cb749fd56f76f2ed4f58bc34db3ca7ea5d31fbab0403850e50315';
    //
    public function index(Request $request)
    {
        //
        // $limit = intval($request->limit);
        // $page = intval($request->page - 1) * $limit;
        if (!$request->filled('key') || !$request->filled('table') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $table = $request->table;
        $year = $request->year;

        $krisnaColumn = Mapping::select('krisna_cols')->where('table', $table)
            ->where('year', $year)
            ->first();

        if (!$krisnaColumn) {
            return response()->json(['success' => false, 'result' => 'Database is not ready'], 400);
        }

        $krisnaCols = explode(",", $krisnaColumn->krisna_cols);

        if (count($krisnaCols) < 2) {
            return response()->json(['success' => false, 'result' => 'Database is not ready'], 400);
        }

        Config::set('database.connections.dbrefprod.database',  'dbref' . $year);

        $kddept = $request->kddept;
        $datadb = DB::connection('dbrefprod')
            ->table($table)
            ->when($kddept, function ($query) use ($kddept) {
                return $query->where('kddept', $kddept);
            })
            ->get();

        $data = [];
        foreach ($datadb as $d) {
            $newdd = new stdClass;

            foreach ($krisnaCols as $kcols) {
                if (property_exists($d, $kcols)) $newdd->{$kcols} = $d->{$kcols};
                else $newdd->{$kcols} = null;
            }

            $data[] = $newdd;
        }

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function volsoutput(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('kddept') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }

        $kddept = $request->kddept;
        $year = $request->year;

        $data = DB::connection('dwh')
            ->table('SPAN_ADK21_R_SOUTPUT')
            ->where('kddept', $kddept)
            ->where('thang', $year)
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function realisasi(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('kddept') || !$request->filled('kdunit') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $year = $request->year;

        $data = DB::connection('dwh')
            ->table('r_' . $year . '_sak')
            ->where('kddept', $request->kddept)
            ->where('kdunit', $request->kdunit)
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function pagu(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('kddept') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $kddept = $request->kddept;
        $year = $request->year;

        $data = DB::connection('dwh')
            ->table('p_' . $year . '_rev')
            ->where('kddept', $kddept)
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function kpjm(Request $request)
    {
        if (!$request->filled('key') || !$request->has('kddept') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $kddept = $request->kddept;
        $year = $request->year;

        if (!DB::connection('dwh')->getSchemaBuilder()->hasTable('p_' . $year . '_cur_kpjm_pi'))
            return response()->json(['success' => false, 'result' => 'Table does not exist'], 500);

        if ($request->filled('kddept')) {
            $data = DB::connection('dwh')
                ->table('p_' . $year . '_cur_kpjm_pi')
                ->where('kddept', $kddept)
                ->get();
        } else {
            $data = DB::connection('dwh')
                ->table('p_' . $year . '_cur_kpjm_pi')
                ->get();
        }

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function volume(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('kddept') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $kddept = $request->kddept;
        $year = $request->year;

        $data = DB::connection('dwh')
            ->table('p_' . $year . '_soutput_rev')
            ->where('kddept', $kddept)
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function lokasi(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('year')) return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $year = $request->year;
        Config::set('database.connections.dbrefprod.database',  'dbref' . $year);
        $data = DB::connection('dbrefprod')
            ->table('t_lokasi')
            ->select('kdlokasi', 'nmlokasi')
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function kabkota(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('year')) return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $year = $request->year;
        Config::set('database.connections.dbrefprod.database',  'dbref' . $year);
        $data = DB::connection('dbrefprod')
            ->table('t_kabkota')
            ->select('kdlokasi', 'kdkabkota', 'nmkabkota')
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }
}
