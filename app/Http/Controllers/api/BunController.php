<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Mapping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use stdClass;

class BunController extends Controller
{
    private $key = '2a12j0DHZXk3OQAPpijljmNWmu7jfJ5XFyUSvkd2gax04VsFMlz2PXLa';

    public function rev(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $year = $request->year;

        $data = DB::connection('dwh')
            ->table('p_' . $year . '_rev_bun')
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }

    public function soutput(Request $request)
    {
        if (!$request->filled('key') || !$request->filled('year')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $year = $request->year;

        $data = DB::connection('dwh')
            ->table('p_' . $year . '_soutput_rev_bun')
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }
}
