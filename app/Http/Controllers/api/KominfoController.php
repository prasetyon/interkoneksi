<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Mapping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use stdClass;

class KominfoController extends Controller
{
    private $key = '2a12NXI9Ydp1DhiG2oboLMqW7ejC68FPVnRgEMXpe9tUJlNAp4mk3k2u';
    //
    public function ega(Request $request)
    {
        //
        // $limit = intval($request->limit);
        // $page = intval($request->page - 1) * $limit;
        if (!$request->filled('key') || !$request->filled('table') || !$request->filled('year') || !$request->filled('kddept')) {
            return response()->json(['success' => false, 'result' => 'Missing Parameter'], 422);
        }
        if ($request->key != $this->key) return response()->json(['success' => false, 'result' => 'Invalid Key'], 422);

        $table = $request->table;
        $year = $request->year;


        $sourceTable = ($table == 'item' ? 'd_' : 't_').$table.'_ega_'.$year;
        $checkTable = DB::connection('dwh')->getSchemaBuilder()->hasTable($sourceTable);

        if (!$checkTable) {
            return response()->json(['success' => false, 'result' => 'Database is not ready'], 400);
        }

        $data = DB::connection('dwh')
            ->table($sourceTable)
            ->when($request->filled('kddept'), function ($query) use ($request) {
                $query->where('kddept', $request->kddept);
            })
            ->when($request->filled('kdunit') && $table != 'dept', function ($query) use ($request) {
                $query->where('kdunit', $request->kdunit);
            })
            ->when($request->filled('kdprogram') && in_array($table, ['program', 'giat', 'output', 'soutput', 'item']), function ($query) use ($request) {
                $query->where('kdprogram', $request->kdprogram);
            })
            ->when($request->filled('kdgiat') && in_array($table, ['giat', 'output', 'soutput', 'item']), function ($query) use ($request) {
                $query->where('kdgiat', $request->kdgiat);
            })
            ->when($request->filled('kdoutput') && in_array($table, ['output', 'soutput', 'item']), function ($query) use ($request) {
                $query->where('kdoutput', $request->kdoutput);
            })
            ->when($request->filled('kdsoutput') && in_array($table, ['soutput', 'item']), function ($query) use ($request) {
                $query->where('kdsoutput', $request->kdsoutput);
            })
            ->get();

        return response()->json(['success' => true, 'result' => 'Successfully Get ' . count($data) . ' Data', 'data' => $data], 200);
    }
}
