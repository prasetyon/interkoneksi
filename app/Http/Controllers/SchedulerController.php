<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Sync;
use App\Mapping;
use App\Scheduler;
use App\Category;
use Illuminate\Support\Facades\Artisan;

class SchedulerController extends Controller
{
    //
    public function index(Request $request)
    {
        $year = $request->session()->get('year');
        $syncz = Sync::where('year', $year)->selectRaw('`table`, max(created_at) as last_update')
            ->where('source', 'krisna')->where('dest', 'dbref')
            ->groupBy('table');

        $synczz = Sync::where('year', $year)->select('table', 'status', 'created_at')
            ->where('source', 'krisna')->where('dest', 'dbref');

        $data['mapping'] = Mapping::where('year', $year)->leftJoinSub($syncz, 'a', function ($join) {
            $join->on('mappings.table', '=', 'a.table');
        })->leftJoinSub($synczz, 'b', function ($join) {
            $join->on('b.table', '=', 'a.table');
            $join->on('b.created_at', '=', 'a.last_update');
        })->select('mappings.*', 'a.last_update', 'b.status')->get();

        return view('scheduler')->with('data', $data)->with('menu', 'sched');
    }

    public function index2(Request $request)
    {
        $year = $request->session()->get('year');
        $syncz = Sync::where('year', $year)->selectRaw('`table`, max(created_at) as last_update')
            ->where('source', 'dbref')->where('dest', 'dbrefprod')
            ->groupBy('table');

        $data['mapping'] = Scheduler::where('year', $year)->leftJoinSub($syncz, 'a', function ($join) {
            $join->on('schedulers.table', '=', 'a.table');
        })->select('schedulers.*', 'a.last_update')->get();

        return view('scheduler2')->with('data', $data)->with('menu', 'sched2');
    }

    public function switchStatus(Request $request)
    {
        $year = $request->session()->get('year');
        // dd($request);
        $update = Mapping::where('id', $request->id)
            ->update(['scheduler_status' => $request->status == 'on' ? 1 : 0]);

        return redirect()->route('scheduler');
    }

    public function switchStatus2(Request $request)
    {
        $year = $request->session()->get('year');
        // dd($request);
        $update = Scheduler::where('id', $request->id)
            ->update(['status' => $request->status == 'on' ? 1 : 0]);

        return redirect()->route('scheduler2');
    }

    public function update(Request $request)
    {
        $year = $request->session()->get('year');
        // dd($request);
        $update = Mapping::where('id', $request->id)
            ->update([
                'scheduler_time' => $request->jadwal,
                'api' => $request->api,
                'upsert' => $request->upsert,
                'delete' => $request->deletez,
                'delete_remain' => $request->deleter,
                'updater' => $request->updater
            ]);

        return redirect()->route('scheduler');
    }

    public function update2(Request $request)
    {
        $year = $request->session()->get('year');
        // dd($request);
        $update = Scheduler::where('id', $request->id)
            ->update(['time' => $request->jadwal]);

        return redirect()->route('scheduler2');
    }

    public function execute(Request $request)
    {
        $year = $request->session()->get('year');
        $data['category'] = Category::all();
        foreach ($data['category'] as $cat) {
            DB::disconnect($cat->db);
            Config::set('database.connections.' . $cat->db . '.database',  $cat->schema . $year);
        }

        // dd($request);
        $map = Mapping::where('id', $request->id)->first();
        $exitCode = Artisan::call($map->scheduler, ['year' => $year, 'url' => $map->api]);

        if ($exitCode > 0) {
            $refcount = DB::connection('dbref')->table($map->table)->get()->count();

            $sync = new Sync();
            $sync->table = $map->table;
            $sync->source = $map->source;
            $sync->year = $year;
            $sync->dest = 'dbref';
            $sync->status = 'manual|' . 'Ref: ' . $refcount . ' Krisna: ' . $exitCode;
            $sync->save();
        }

        // dd(Artisan::output());

        return redirect()->route('scheduler');
    }
}
