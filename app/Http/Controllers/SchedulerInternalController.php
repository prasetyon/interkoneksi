<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Sync;
use App\InternalScheduler;
use App\Category;
use Artisan;

class SchedulerInternalController extends Controller
{
    //
    public function index(Request $request)
    {
        $year = $request->session()->get('year');

        $syncz = Sync::where('year', $year)->selectRaw('`table`, max(created_at) as last_update')
            ->where('source', 'Internal Scheduler')
            ->groupBy('table');

        $synczz = Sync::where('year', $year)->select('table', 'status', 'created_at')
            ->where('source', 'Internal Scheduler');

        $data['scheduler'] = InternalScheduler::where('year', $year)->leftJoinSub($syncz, 'a', function ($join) {
            $join->on('internal_schedulers.name', '=', 'a.table');
        })->leftJoinSub($synczz, 'b', function ($join) {
            $join->on('b.table', '=', 'a.table');
            $join->on('b.created_at', '=', 'a.last_update');
        })->select('internal_schedulers.*', 'a.last_update', 'b.status as sync_status')->get();

        return view('schedulerinternal')->with('data', $data)->with('menu', 'schedinter');
    }

    public function switchStatus(Request $request)
    {
        $year = $request->session()->get('year');
        // dd($request);
        $update = InternalScheduler::where('id', $request->id)
            ->update(['status' => $request->status == 'on' ? 1 : 0]);

        return redirect()->route('schedinter');
    }

    public function update(Request $request)
    {
        $year = $request->session()->get('year');
        // dd($request);
        $update = InternalScheduler::where('id', $request->id)
            ->update(['time' => $request->jadwal]);

        return redirect()->route('schedinter');
    }

    public function execute(Request $request)
    {
        $year = $request->session()->get('year');

        DB::disconnect('dbref');
        Config::set('database.connections.dbref.database',  'test' . $year);
        $exitCode = Artisan::call($request->signature, ['year' => $year]);
        \Log::info('ExitCode: ' . $exitCode);

        if ($exitCode > 0) {
            $sync = new Sync();
            $sync->table = $request->name;
            $sync->source = 'Internal Scheduler';
            $sync->year = $year;
            $sync->dest = 'Internal Scheduler';
            $sync->status = 'manual';
            $sync->save();
        }

        return redirect()->route('schedinter');
    }
}
