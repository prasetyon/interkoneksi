<?php

namespace App\Helpers;

use App\Mapping;

class Helper
{
    public static function updateKrisnaColumn($table, $year, $col)
    {
        Mapping::where('table', $table)
            ->where('year', $year)
            ->update([
                'krisna_cols' => implode(",", array_keys(get_object_vars($col)))
            ]);
    }

    public static function getMappingInfo($table, $year)
    {
        return Mapping::where('year', $year)
            ->where('table', $table)
            ->first();
    }
}
