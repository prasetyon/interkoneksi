<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetEs2 extends Command
{
    protected $signature = "scheduler:getEs2 {year} {url}";
    protected $description = "Update Es2 Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get Es2 from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_es2', $this->argument('year'));
            // dd($data);

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_es2')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_es2', $this->argument('year'), $d);
                    }
                    $totalData++;

                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $delz = DB::connection('dbref')->table('t_es2')
                            ->where('kddept', $d->kddept)
                            ->where('kdunit', $d->kdunit)
                            ->where('kdes2', $d->kdes2)
                            ->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        DB::connection('dbref')->table('t_es2')->upsert(
                            [
                                'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdes2' => $d->kdes2, 'nmes2' => $d->nmes2,
                                'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                            ],
                            ['kddept', 'kdunit', 'kdes2'],
                            ['nmes2', 'kddept', 'kdunit', 'kdupdate' => 'U', 'updater' => 'Scheduler WS', 'tglupdate' => now(), 'kdes2']
                        );
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_es2')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        // dd($returnCode);
        Log::info('Finished running scheduler get Es2 from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
