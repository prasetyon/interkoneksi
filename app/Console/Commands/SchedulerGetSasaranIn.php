<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetSasaranIn extends Command
{
    protected $signature = "scheduler:getSasaranIn {year} {url}";
    protected $description = "Update SasaranIn Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get SasaranIn from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_sasaranin', $this->argument('year'));

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_sasaranin')
                        ->update(['kdupdate' => 'Z']);
                }

                $table = array(
                    '≤' => ' <= ', '≥' => ' >= ',
                );

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_sasaranin', $this->argument('year'), $d);
                    }
                    $totalData++;

                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $delz = DB::connection('dbref')->table('t_sasaranin')
                            ->where('kddept', $d->kddept)
                            ->where('kdsasaran', $d->kdsasaran)
                            ->where('kdindi', $d->kdindi)
                            ->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        $d->target_0 = strtr($d->target_0, $table);
                        $d->target_0 = preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_0);
                        $d->target_1 = strtr($d->target_1, $table);
                        $d->target_1 = preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_1);
                        $d->target_2 = strtr($d->target_2, $table);
                        $d->target_2 = preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_2);
                        $d->target_3 = strtr($d->target_3, $table);
                        $d->target_3 = preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_3);
                        $d->nmindi = strtr($d->nmindi, $table);
                        $d->nmindi = preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->nmindi);

                        DB::connection('dbref')->table('t_sasaranin')->upsert(
                            [
                                'kddept' => $d->kddept, 'kdsasaran' => $d->kdsasaran,
                                'kdindi' => $d->kdindi, 'satuan' => substr($d->satuan, 0, 30),
                                'nmindi' => substr(preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->nmindi), 0, 250),
                                'target_0' => substr(preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_0), 0, 30),
                                'target_1' => substr(preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_1), 0, 30),
                                'target_2' => substr(preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_2), 0, 30),
                                'target_3' => substr(preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $d->target_3), 0, 30)
                            ],
                            ['kddept', 'kdsasaran', 'kdindi'],
                            ['nmindi', 'satuan', 'target_0', 'target_1', 'target_2', 'target_3']
                        );
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_sasaranin')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get SasaranIn from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
