<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetLokasiSubOutput extends Command
{
    protected $signature = "scheduler:getLokasiSoutput {year} {url}";
    protected $description = "Update Lokasi Sub Output Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get Lokasi Sub Output from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_lokasi_soutput', $this->argument('year'));
            // dd($data);

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_lokasi_soutput')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_lokasi_soutput', $this->argument('year'), $d);
                    }
                    $totalData++;

                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        if (intval($this->argument('year')) == 2021) {
                            $whereArray = array(
                                'kdprogram' => $d->kdprogram,
                                'kdgiat' => $d->kdgiat,
                                'kdoutput' => $d->kdoutput,
                                'kdsoutput' => $d->kdsoutput,
                                'kdlokasisoutput' => $d->kdlokasisoutput,
                            );
                        } else {
                            $whereArray = array(
                                'kdprogram' => $d->kdprogram,
                                'kdgiat' => $d->kdgiat,
                                'kdoutput' => $d->kdoutput,
                                'kdsoutput' => $d->kdsoutput,
                                'kdlokasisoutput' => $d->kdlokasisoutput,
                                'ref_tabel' => $d->ref_table,
                            );
                        }

                        $query = DB::connection('dbref')->table('t_lokasi_soutput');

                        foreach ($whereArray as $field => $value) {
                            $query->where($field, $value);
                        }

                        $query->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        if (intval($this->argument('year')) == 2021) {
                            DB::connection('dbref')->table('t_lokasi_soutput')->upsert(
                                [
                                    'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat, 'kdsoutput' => $d->kdsoutput, 'kdoutput' => $d->kdoutput, 'kdlokasisoutput' => $d->kdlokasisoutput,
                                    'nmlokasisoutput' => substr($d->nmlokasisoutput, 0, 20), 'wilayah_id' => $d->wilayah_id, 'target_0' => substr($d->target_0, 0, 20),
                                    'target_1' => substr($d->target_1, 0, 20), 'target_2' => substr($d->target_2, 0, 20), 'target_3' => substr($d->target_3, 0, 20),
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                                ],
                                ['kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput', 'kdlokasisoutput'],
                                [
                                    'nmlokasisoutput', 'wilayah_id', 'target_0', 'target_1', 'target_2', 'target_3', 'kdupdate' => 'U', 'updater', 'tglupdate'
                                ]
                            );
                        } else {
                            DB::connection('dbref')->table('t_lokasi_soutput')->upsert(
                                [
                                    'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat, 'kdsoutput' => $d->kdsoutput, 'kdoutput' => $d->kdoutput, 'kdlokasisoutput' => $d->kdlokasisoutput,
                                    'nmlokasisoutput' => substr($d->nmlokasisoutput, 0, 20), 'wilayah_id' => $d->wilayah_id, 'target_0' => substr($d->target_0, 0, 20),
                                    'target_1' => substr($d->target_1, 0, 20), 'target_2' => substr($d->target_2, 0, 20), 'target_3' => substr($d->target_3, 0, 20),
                                    'ref_tabel' => $d->ref_table, 'alokasi_total' => $d->alokasi_total,
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                                ],
                                ['kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput', 'kdlokasisoutput', 'ref_tabel'],
                                [
                                    'nmlokasisoutput', 'wilayah_id', 'target_0', 'target_1', 'target_2', 'target_3', 'alokasi_total', 'kdupdate' => 'U', 'updater', 'tglupdate'
                                ]
                            );
                        }
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_lokasi_soutput')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        // dd($returnCode);
        Log::info('Finished running scheduler get Lokasi Sub Output from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
