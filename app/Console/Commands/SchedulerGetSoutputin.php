<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\InternalScheduler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetSoutputIn extends Command
{
    protected $signature = "scheduler:getSoutputIn {year} {url}";
    protected $description = "Update Suboutput In Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get SubOutput In from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_soutputin', $this->argument('year'));

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_soutputin')
                        ->where('kddept', '<>', '999')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_soutputin', $this->argument('year'), $d);
                    }
                    $totalData++;

                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $whereArray = array(
                            'kddept' => $d->kddept,
                            'kdunit' => $d->kdunit,
                            'kdprogram' => $d->kdprogram,
                            'kdgiat' => $d->kdgiat,
                            'kdoutput' => $d->kdoutput,
                            'kdindi' => $d->kdindi
                        );

                        $query = DB::connection('dbref')->table('t_soutputin');

                        foreach ($whereArray as $field => $value) {
                            $query->where($field, $value);
                        }

                        $query->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        if($d->thang >= 2023){
                            DB::connection('dbref')->table('t_soutputin')->upsert(
                                [
                                    'thang' => $d->thang, 'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdes2' => $d->kdes2, 'kdsoutput' => $d->kdsoutput,
                                    'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat, 'kdoutput' => $d->kdoutput, 'nmindi' => substr($d->nmindi, 0, 250),
                                    'kdindi' => substr($d->kdindi, 0, 2), 'satuan' => substr($d->satuan, 0, 30), 'target_0' => substr($d->target_0, 0, 30),
                                    'target_1' => substr($d->target_1, 0, 30), 'target_2' => substr($d->target_2, 0, 30), 'target_3' => substr($d->target_3, 0, 30),
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(),
                                    'submitted' => $d->submitted, 'approved' => $d->approved, 'suspended' => $d->suspended
                                ],
                                [
                                    'thang', 'kddept', 'kdunit', 'kdes2', 'kdprogram', 'kdgiat', 'kdoutput', 
                                    'kdsoutput', 'kdindi'
                                ],
                                [
                                    'nmindi', 'satuan', 'target_0', 'target_1', 'target_2', 'target_3', 
                                    'kdupdate' => 'U', 'kdes2', 'updater', 'tglupdate',
                                    'submitted', 'approved', 'suspended'
                                ]
                            );
                        } else {
                            DB::connection('dbref')->table('t_soutputin')->upsert(
                                [
                                    'thang' => $d->thang, 'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdes2' => $d->kdes2, 'kdsoutput' => $d->kdsoutput,
                                    'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat, 'kdoutput' => $d->kdoutput, 'nmindi' => substr($d->nmindi, 0, 250),
                                    'kdindi' => substr($d->kdindi, 0, 2), 'satuan' => substr($d->satuan, 0, 30), 'target_0' => substr($d->target_0, 0, 30),
                                    'target_1' => substr($d->target_1, 0, 30), 'target_2' => substr($d->target_2, 0, 30), 'target_3' => substr($d->target_3, 0, 30),
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                                ],
                                [
                                    'thang', 'kddept', 'kdunit', 'kdes2', 'kdprogram', 'kdgiat', 'kdoutput', 
                                    'kdsoutput', 'kdindi'
                                ],
                                [
                                    'nmindi', 'satuan', 'target_0', 'target_1', 'target_2', 'target_3', 
                                    'kdupdate' => 'U', 'kdes2', 'updater', 'tglupdate'
                                ]
                            );
                        }
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_soutputin')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get Soutput In from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        $checkSat = InternalScheduler::where('name', 'like', '%sat%')
            ->where('year', $this->argument('year'))
            ->first();

        if ($checkSat && $checkSat->status == 1) {
            Artisan::call('internalscheduler:sat1', ['year' => $this->argument('year')]);
        }

        return $returnCode;
    }
}
