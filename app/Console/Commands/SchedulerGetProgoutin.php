<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetProgoutin extends Command
{
    protected $signature = "scheduler:getProgOutIn {year} {url}";
    protected $description = "Update Progoutin Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get Progoutin from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_progoutin', $this->argument('year'));

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_progoutin')
                        ->where('kddept', '<>', '999')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_progoutin', $this->argument('year'), $d);
                    }
                    $totalData++;

                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $whereArray = array(
                            'kddept' => $d->kddept,
                            'kdunit' => $d->kdunit,
                            'kdprogram' => $d->kdprogram,
                            'kdprogout' => $d->kdprogout,
                            'kdindi' => $d->kdindi
                        );

                        $query = DB::connection('dbref')->table('t_progoutin');

                        foreach ($whereArray as $field => $value) {
                            $query->where($field, $value);
                        }

                        $query->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        if(intval($this->argument('year')) >= 2023){
                            DB::connection('dbref')->table('t_progoutin')->upsert(
                                [
                                    'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdprogram' => $d->kdprogram, 'kdprogout' => $d->kdprogout,
                                    'nmindi' => substr($d->nmindi, 0, 250), 'kdindi' => $d->kdindi, 'satuan' => substr($d->satuan, 0, 30), 'target_0' => substr($d->target_0, 0, 30),
                                    'target_1' => substr($d->target_1, 0, 30), 'target_2' => substr($d->target_2, 0, 30), 'target_3' => substr($d->target_3, 0, 30),
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(), 'thang' => $this->argument('year'),
                                    'submitted' => $d->submitted, 'approved' => $d->approved, 'suspended' => $d->suspended
                                ],
                                [
                                    'kddept', 'kdunit', 'kdprogram', 'kdprogout', 'kdindi'
                                ],
                                [
                                    'nmindi', 'satuan', 'target_0', 'target_1', 'target_2', 'target_3', 
                                    'kdupdate' => 'U', 'updater', 'tglupdate', 'thang',
                                    'submitted', 'approved', 'suspended'
                                ]
                            );
                        }
                        else if (intval($this->argument('year')) >= 2021) {
                            DB::connection('dbref')->table('t_progoutin')->upsert(
                                [
                                    'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdprogram' => $d->kdprogram, 'kdprogout' => $d->kdprogout,
                                    'nmindi' => substr($d->nmindi, 0, 250), 'kdindi' => $d->kdindi, 'satuan' => substr($d->satuan, 0, 30), 'target_0' => substr($d->target_0, 0, 30),
                                    'target_1' => substr($d->target_1, 0, 30), 'target_2' => substr($d->target_2, 0, 30), 'target_3' => substr($d->target_3, 0, 30),
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(), 'thang' => $this->argument('year')
                                ],
                                [
                                    'kddept', 'kdunit', 'kdprogram', 'kdprogout', 'kdindi'
                                ],
                                [
                                    'nmindi', 'satuan', 'target_0', 'target_1', 'target_2', 'target_3', 
                                    'kdupdate' => 'U', 'updater', 'tglupdate', 'thang',
                                ]
                            );
                        } else {
                            DB::connection('dbref')->table('t_progoutin')->upsert(
                                [
                                    'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdprogram' => $d->kdprogram, 'kdprogout' => $d->kdprogout,
                                    'nmindi' => substr($d->nmindi, 0, 250), 'kdindi' => $d->kdindi, 'satuan' => substr($d->satuan, 0, 30), 'target_0' => substr($d->target_0, 0, 30),
                                    'target_1' => substr($d->target_1, 0, 30), 'target_2' => substr($d->target_2, 0, 30), 'target_3' => substr($d->target_3, 0, 30)
                                ],
                                [
                                    'kddept', 'kdunit', 'kdprogram', 'kdprogout', 'kdindi'
                                ],
                                [
                                    'nmindi', 'satuan', 'target_0', 'target_1', 'target_2', 'target_3'
                                ]
                            );
                        }
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_progoutin')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get Progoutin from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
