<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\InternalScheduler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetSubOutput extends Command
{
    protected $signature = "scheduler:getSubOutput {year} {url}";
    protected $description = "Update Sub Output Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get Sub Output from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        $dept = DB::connection('dbref')
            ->table('t_dept')
            ->orderBy('kddept')
            ->get();

        $totalData = $upsert = $del = $stay = 0;

        $map = Helper::getMappingInfo('t_soutput', $this->argument('year'));

        if ($map->delete_remain == 1 && $map->updater == 1) {
            DB::connection('dbref')->table('t_soutput')
                ->update(['kdupdate' => 'Z']);
        }

        foreach ($dept as $dep) {
            Log::info('Get sub output form kddept: ' . $dep->kddept);
            curl_setopt($curl, CURLOPT_URL, $this->argument('url') . '&kddept=' . $dep->kddept);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $response = curl_exec($curl);

            $err = curl_error($curl);

            if ($err) {
                echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
                echo "cURL Error #:" . $err;
                return -1;
            } else {
                $data = json_decode($response);

                if ($data->success) {
                    Log::info('Get data success');

                    if ($this->argument('year') > 2020) {
                        $kro = DB::connection('dbref')->table('t_kro')->select('kdoutput')->get();
                        $i = 0;
                        foreach ($kro as $kk) {
                            $arrKro[$i++] = $kk->kdoutput;
                        }
                    }

                    $returnCode = count($data->data);

                    foreach ($data->data as $key => $d) {
                        if ($key == 0) {
                            Helper::updateKrisnaColumn('t_soutput', $this->argument('year'), $d);
                        }
                        $totalData++;

                        if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                            $whereArray = array(
                                'kddept' => $d->kddept,
                                'kdunit' => $d->kdunit,
                                'kdprogram' => $d->kdprogram,
                                'kdgiat' => $d->kdgiat,
                                'kdoutput' => $d->kdoutput,
                                'kdsoutput' => $d->kdsoutput
                            );

                            $query = DB::connection('dbref')->table('t_soutput');

                            foreach ($whereArray as $field => $value) {
                                $query->where($field, $value);
                            }

                            $query->delete();
                            $del++;
                        } else if (($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) /*&& in_array($d->kdoutput, $arrKro)*/) {      
                            if($d->thang >= 2023){
                                DB::connection('dbref')->table('t_soutput')->upsert(
                                    [
                                        'thang' => $d->thang, 'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdmp' => $d->kdmp,
                                        'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat, 'kdsoutput' => $d->kdsoutput,
                                        'kdpn' => $d->kdpn, 'kdpp' => $d->kdpp, 'kdkp' => $d->kdkp, 'kdproy' => $d->kdproy,
                                        'kdjanpres' => $d->kdjanpres, 'kdnawacita' => $d->kdnawacita, 'kdtema' => $d->kdtema, 'kdsaskeg' => $d->kdsaskeg,
                                        'kdoutput' => $d->kdoutput, 'nmsoutput' => preg_replace('/\s+/', ' ', substr($d->nmsoutput, 0, 249)),
                                        'sat1' => $d->sat, 'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(),
                                        'submitted' => $d->submitted, 'approved' => $d->approved, 'suspended' => $d->suspended
                                    ],
                                    [
                                        'thang', 'kddept', 'kdunit', 'kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput'
                                    ],
                                    [
                                        'nmsoutput', 'kdupdate' => 'U', 'updater', 'tglupdate', 'kdpn', 'kdpp', 
                                        'kdmp', 'kdkp', 'kdproy',
                                        'kdsaskeg', 'kdjanpres', 'kdnawacita', 'kdtema', 'sat1', 'thang',
                                        'submitted', 'approved', 'suspended'
                                    ]
                                );
                            } else {
                                DB::connection('dbref')->table('t_soutput')->upsert(
                                    [
                                        'thang' => $d->thang, 'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdmp' => $d->kdmp,
                                        'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat, 'kdsoutput' => $d->kdsoutput,
                                        'kdpn' => $d->kdpn, 'kdpp' => $d->kdpp, 'kdkp' => $d->kdkp, 'kdproy' => $d->kdproy,
                                        'kdjanpres' => $d->kdjanpres, 'kdnawacita' => $d->kdnawacita, 'kdtema' => $d->kdtema, 'kdsaskeg' => $d->kdsaskeg,
                                        'kdoutput' => $d->kdoutput, 'nmsoutput' => preg_replace('/\s+/', ' ', substr($d->nmsoutput, 0, 249)),
                                        'sat1' => $d->sat, 'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(),
                                    ],
                                    [
                                        'thang', 'kddept', 'kdunit', 'kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput'
                                    ],
                                    [
                                        'nmsoutput', 'kdupdate' => 'U', 'updater', 'tglupdate', 'kdpn', 'kdpp', 
                                        'kdmp', 'kdkp', 'kdproy',
                                        'kdsaskeg', 'kdjanpres', 'kdnawacita', 'kdtema', 'sat1', 'thang',
                                        
                                    ]
                                );
                            }
                            $upsert++;
                        } else {
                            $stay++;
                        }
                    }
                } else {
                    Log::info('Get data erorr with log: ' . $data->message);
                    return -1;
                }
            }
        }

        if ($map->delete_remain == 1 && $map->updater == 1) {
            $delz = DB::connection('dbref')->table('t_soutput')
                ->where('kdupdate', 'Z')->delete();
            $del += $delz;
            $stay -= $delz;
        }

        $checkSat = InternalScheduler::where('name', 'like', '%sat%')
            ->where('year', $this->argument('year'))
            ->first();

        if ($checkSat && $checkSat->status == 1) {
            DB::connection('dbref')->table('t_soutput')
                ->whereNull('sat')
                ->orWhere('sat', '')
                ->update(['sat' => DB::raw('`sat1`')]);
        }

        curl_close($curl);

        Log::info('Finished running scheduler get Sub Output from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);
        return $returnCode;
    }
}
