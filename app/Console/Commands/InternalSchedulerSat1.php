<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InternalSchedulerSat1 extends Command
{
    protected $signature = "internalscheduler:sat1 {year}";
    protected $description = "Update Data Cadangan";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running internal scheduler update data sat1 ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        DB::connection('dbref')->table('t_soutput')
            ->update(['sat' => DB::raw('`sat1`')]);

        $data = DB::connection('dbref')->table('t_soutputin')->get();

        foreach ($data as $d) {
            DB::connection('dbref')->table('t_soutput')
                ->where('kddept', $d->kddept)
                ->where('kdunit', $d->kdunit)
                ->where('kdprogram', $d->kdprogram)
                ->where('kdgiat', $d->kdgiat)
                ->where('kdoutput', $d->kdoutput)
                ->where('kdsoutput', $d->kdsoutput)
                ->update(['sat' => $d->satuan]);
        }

        DB::connection('dbref')->table('t_soutput')
            ->whereNull('sat')
            ->orWhere('sat', '')
            ->update(['sat' => DB::raw('`sat1`')]);

        Log::info('Finished running internal scheduler update data sat1');
        return 1;
    }
}
