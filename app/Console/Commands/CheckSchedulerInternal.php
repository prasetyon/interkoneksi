<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Mapping;
use App\Category;
use App\InternalScheduler;
use App\Sync;
use App\Scheduler;
use Datetime;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class CheckSchedulerInternal extends Command
{
    protected $signature = 'check:internalscheduler';
    protected $description = 'Command description';

    public function handle()
    {
        //
        date_default_timezone_set('Asia/Jakarta');
        $current = date("H:i");
        Log::info('Checking Internal Scheduler Running on ' . $current);

        $map = InternalScheduler::where('status', 1)->where('time', 'LIKE', '%' . $current . '%')->get();
        foreach ($map as $data) {
            DB::disconnect('dbref');
            Config::set('database.connections.dbref.database',  'test' . $data->year);
            $exitCode = Artisan::call($data->signature, ['year' => $data->year]);
            Log::info('ExitCode: ' . $exitCode);

            if ($exitCode > 0) {
                $sync = new Sync();
                $sync->table = $data->name;
                $sync->source = 'Internal Scheduler';
                $sync->year = $data->year;
                $sync->dest = 'Internal Scheduler';
                $sync->status = 'auto';
                $sync->save();
            }
        }
    }
}
