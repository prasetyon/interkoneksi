<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetKegiatan extends Command
{
    protected $signature = "scheduler:getGiat {year} {url}";
    protected $description = "Update Kegiatan Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get kegiatan from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_giat', $this->argument('year'));
            // dd($data);

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_giat')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_giat', $this->argument('year'), $d);
                    }
                    $totalData++;
                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $whereArray = array('kdgiat' => $d->kdgiat);

                        $query = DB::connection('dbref')->table('t_giat');

                        foreach ($whereArray as $field => $value) {
                            $query->where($field, $value);
                        }

                        $query->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        if(intval($this->argument('year')) >= 2023){
                            DB::connection('dbref')->table('t_giat')->upsert(
                                [
                                    'thang' => $this->argument('year'), 'kdgiat' => $d->kdgiat, 'nmgiat' => substr($d->nmgiat, 0, 250),
                                    'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdes2' => $d->kdes2, 'kdprogout' => $d->kdprogout ?? "",
                                    'kdprogram' => $d->kdprogram, 'kdfungsi' => $d->kdfungsi, 'kdsfung' => $d->kdsfung,
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(),
                                    'submitted' => $d->submitted, 'approved' => $d->approved, 'suspended' => $d->suspended
                                ],
                                ['thang', 'kdgiat'],
                                [
                                    'nmgiat', 'kddept', 'kdunit', 'kdprogram', 'kdfungsi', 'kdsfung', 'kdes2', 'kdprogout',
                                    'kdupdate' => 'U', 'updater', 'tglupdate',
                                    'submitted', 'approved', 'suspended'
                                ]
                            );
                        } else {
                            DB::connection('dbref')->table('t_giat')->upsert(
                                [
                                    'thang' => $this->argument('year'), 'kdgiat' => $d->kdgiat, 'nmgiat' => substr($d->nmgiat, 0, 250),
                                    'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdes2' => $d->kdes2, 'kdprogout' => $d->kdprogout ?? "",
                                    'kdprogram' => $d->kdprogram, 'kdfungsi' => $d->kdfungsi, 'kdsfung' => $d->kdsfung,
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                                ],
                                ['thang', 'kdgiat'],
                                [
                                    'nmgiat', 'kddept', 'kdunit', 'kdprogram', 'kdfungsi', 'kdsfung', 'kdes2', 'kdprogout',
                                    'kdupdate' => 'U', 'updater', 'tglupdate'
                                ]
                            );
                        }
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_giat')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get kegiatan from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
