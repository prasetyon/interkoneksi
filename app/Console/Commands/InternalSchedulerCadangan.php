<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InternalSchedulerCadangan extends Command
{
    protected $signature = "internalscheduler:cadangan {year}";
    protected $description = "Update Data Cadangan";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running internal scheduler update data cadangan ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $data = DB::connection('dbref')->table('t_giat')->selectRaw('kddept, kdunit, kdprogram, kdgiat')->get();

        foreach ($data as $d) {
            DB::connection('dbref')->table('t_output')->upsert(
                [
                    'thang' => $this->argument('year'), 'kddept' => $d->kddept, 'kdunit' => $d->kdunit,
                    'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat,
                    'kdoutput' => '999', 'nmoutput' => 'Output Cadangan', 'sat' => 'cadangan', 'kdsum' => 1,
                    'kdupdate' => 'C', 'updater' => 'Scheduler Cadangan', 'tglupdate' => now(), 'aktif' => 1
                ],
                ['kddept', 'kdunit', 'kdprogram', 'kdgiat', 'kdoutput'],
                ['nmoutput', 'sat', 'aktif']
            );

            DB::connection('dbref')->table('t_soutput')->upsert(
                [
                    'thang' => $this->argument('year'), 'kddept' => $d->kddept, 'kdunit' => $d->kdunit,
                    'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat,
                    'kdoutput' => '999', 'kdsoutput' => '999', 'nmsoutput' => 'Cadangan', 'sat' => 'cadangan', 'sat1' => 'cadangan',
                    'kddefault' => '', 'kdupdate' => 'C', 'updater' => 'Scheduler Cadangan', 'tglupdate' => now(), 'aktif' => 1
                ],
                ['kddept', 'kdunit', 'kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput'],
                ['nmsoutput', 'sat', 'sat1', 'aktif']
            );

            DB::connection('dbref')->table('t_kmpnen')->upsert(
                [
                    'thang' => $this->argument('year'), 'kddept' => $d->kddept, 'kdunit' => $d->kdunit,
                    'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat,
                    'kdoutput' => '999', 'kdsoutput' => '999', 'kdkmpnen' => '999',
                    'nmkmpnen' => 'Cadangan', 'aktif' => 1,
                    'kdupdate' => 'C', 'updater' => 'Scheduler Cadangan', 'tglupdate' => now()
                ],
                ['kddept', 'kdunit', 'kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput', 'kdkmpnen'],
                ['nmkmpnen', 'aktif']
            );
        }

        Log::info('Finished running internal scheduler update data cadangan');
        return 1;
    }
}
