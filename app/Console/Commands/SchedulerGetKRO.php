<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetKRO extends Command
{
    protected $signature = "scheduler:getKRO {year} {url}";
    protected $description = "Update KRO Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get KRO from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);
        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_kro', $this->argument('year'));
            // dd($data);

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_kro')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_kro', $this->argument('year'), $d);
                    }
                    $totalData++;
                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $delz = DB::connection('dbref')->table('t_kro')
                            ->where('kdoutput', $d->kdoutput)
                            ->orWhere('kdoutput', $d->kdoutputpn)
                            ->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        DB::connection('dbref')->table('t_kro')->upsert(
                            [
                                'thang' => $this->argument('year'), 'kdoutput' => $d->kdoutput, 'nmoutput' => $d->nmoutput, 'sat' => $d->satuan ?? "",
                                'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                            ],
                            ['kdoutput',],
                            ['nmoutput', 'kdoutput', 'sat', 'kdupdate' => 'U', 'updater', 'tglupdate', 'thang']
                        );

                        if (isset($d->kdoutputpn)) {
                            DB::connection('dbref')->table('t_kro')->upsert(
                                [
                                    'thang' => $this->argument('year'), 'kdoutput' => $d->kdoutputpn, 'nmoutput' => $d->nmoutput, 'sat' => $d->satuan ?? "",
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                                ],
                                ['kdoutput',],
                                ['nmoutput', 'kdoutput', 'sat', 'kdupdate' => 'U', 'updater', 'tglupdate', 'thang']
                            );
                        }
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_kro')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get KRO from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
