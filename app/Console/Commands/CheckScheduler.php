<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Mapping;
use App\Sync;
use App\Category;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class CheckScheduler extends Command
{
    protected $signature = 'check:scheduler';
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //
        date_default_timezone_set('Asia/Jakarta');
        $current = date("H:i");
        Log::info('Checking Scheduler Running on ' . $current);

        $map = Mapping::where('scheduler_status', 1)->where('scheduler_time', 'LIKE', '%' . $current . '%')->orderBy('seq')->get();
        foreach ($map as $data) {
            DB::disconnect('dbref');
            Config::set('database.connections.dbref.database',  'test' . $data->year);
            $exitCode = Artisan::call($data->scheduler, ['year' => $data->year, 'url' => $data->api]);
            Log::info('ExitCode: ' . $exitCode);

            if ($exitCode > 0) {
                $refcount = DB::connection('dbref')->table($data->table)->get()->count();

                $sync = new Sync();
                $sync->table = $data->table;
                $sync->source = $data->source;
                $sync->year = $data->year;
                $sync->dest = 'dbref';
                $sync->status = 'auto|' . 'Ref: ' . $refcount . ' Krisna: ' . $exitCode;
                $sync->save();
            }
        }
    }
}
