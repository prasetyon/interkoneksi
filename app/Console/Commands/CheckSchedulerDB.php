<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Mapping;
use App\Category;
use App\Sync;
use App\Scheduler;
use Illuminate\Support\Facades\Log;

class CheckSchedulerDB extends Command
{
    protected $signature = 'check:dbscheduler';
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //
        date_default_timezone_set('Asia/Jakarta');
        $current = date("H:i");
        Log::info('Checking DB Scheduler Running on ' . $current);

        $map = Scheduler::where('status', 1)->where('time', 'LIKE', '%' . $current . '%')->get();
        foreach ($map as $data) {
            // $exitCode = Artisan::call($data->scheduler);
            Log::info('Running scheduler get ' . $data->table . ' from ' . $data->source . ' to ' . $data->dest . ' ' . $data->year);

            $data['category'] = Category::all();
            foreach ($data['category'] as $cat) {
                DB::disconnect($cat->db);
                Config::set('database.connections.' . $cat->db . '.database',  $cat->schema . $data->year);
            }

            $this->syncTable($data->source, $data->dest, $data->table);

            $sourcezz = DB::connection($data->source)->table($data->table)->get()->count();
            $destzz = DB::connection($data->dest)->table($data->table)->get()->count();

            $sync = new Sync();
            $sync->table = $data->table;
            $sync->source = $data->source;
            $sync->dest = $data->dest;
            $sync->year = $data->year;
            $sync->status = 'auto';
            $sync->save();
            Log::info('Finished running scheduler get ' . $data->table . ' from ' . $data->source . ' to ' . $data->dest);
            Log::info('DB TEST: ' . $sourcezz . ' DB PROD ' . $destzz);
        }
    }

    private function syncTable($db1, $db2, $s)
    {
        $source = Mapping::where('table', $s)->first();
        $key = $source->primarykey;
        $select = $source->select;
        $comp = $source->compare;
        $keyex = explode(', ', $key);
        $compex = explode(', ', $comp);
        $dbrefz = $cwspanz = [];
        $dbrefzz = DB::connection($db1)
            ->table($s)
            ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
            ->orderByRaw('col')
            ->get();
        $i = 0;
        foreach ($dbrefzz as $dd) {
            $dbrefz[$i]['key'] = $dd->col;
            $dbrefz[$i++]['comp'] = $dd->comp;
        }
        $cwspanzz = DB::connection($db2)
            ->table($s)
            ->selectRaw('concat_ws("|",' . $key . ') AS col,concat_ws("|",' . $comp . ') AS comp')
            ->orderByRaw('col')
            ->get();
        $i = 0;
        foreach ($cwspanzz as $cc) {
            $cwspanz[$i]['key'] = $cc->col;
            $cwspanz[$i++]['comp'] = $cc->comp;
        }
        $diffz = $this->findDiff($cwspanz, $dbrefz, $key, $comp, $db1, $db2);

        $nomdiff = $diffz['comp'];
        $coldiff = $diffz['col'];

        $diff1 = DB::connection($db1)->table($s)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->get();
        // $diff2 = DB::connection($db2)->table($s)->whereIn(DB::raw("CONCAT_ws('|'," . $key . ")"), $coldiff)->delete();

        // INSERT DATA DB1 TO DB2
        foreach ($diff1 as $d) {
            $colname = Schema::connection($db1)->getColumnlisting($s);

            $i = 0;
            $qm = $cm = "";
            foreach ($colname as $colz) {
                $qm .= "?,";
                $cm .= ($colz . ",");
                $tval[$i++] = $d->$colz;
            }
            $qm = substr($qm, 0, strlen($qm) - 1);
            $cm = substr($cm, 0, strlen($cm) - 1);

            $qstate = "insert into " . $s . " (" . $cm . ") values (" . $qm . ")";
            DB::connection($db2)->insert($qstate, $tval);
        }

        foreach ($nomdiff as $n) {
            $ii = $iix = 0;
            $wer = array();
            $werzz = array();
            foreach ($keyex as $k) {
                $wer[$ii++] = [$k, '=', $n->$k];
            }

            $ixix = 0;
            foreach ($n->diff as $cx) {
                $werzz[$cx] = $n->$db1[$ixix];
                $ixix++;
            }

            // dd($werzz);

            $update = DB::connection($db2)
                ->table($s)
                ->where($wer)
                ->update($werzz);
        }

        // echo "<pre>"; print_r(DB::connection($db2)->getQueryLog()); echo "</pre>";
        // dd(0);
    }

    private function findDiff($arr1, $arr2, $key, $comp, $db1, $db2)
    {
        // if(count($arr1)<1) return $arr2;
        // if(count($arr2)<1) return $arr1;

        $res['col'] = [];
        $res['comp'] = [];
        $i = $ii = $j = $x = 0;
        $compex = explode(', ', $comp);

        while ($i < count($arr1) && $j < count($arr2)) {
            if ($arr1[$i]['key'] < $arr2[$j]['key']) {
                $res['col'][$x++] = $arr1[$i]['key'];
                $i++;
            } else if ($arr1[$i]['key'] > $arr2[$j]['key']) {
                $res['col'][$x++] = $arr2[$j]['key'];
                $j++;
            } else {
                if (strcmp($arr1[$i]['comp'], $arr2[$j]['comp'])) {
                    // echo 'compare '.$arr1[$i]['comp'].' with '.$arr2[$j]['comp'].'<br/>';

                    $res['comp'][$ii] = new \stdClass();
                    $kk = explode(', ', $key);
                    $idval = explode('|', $arr1[$i]['key']);
                    $zzz = 0;
                    foreach ($kk as $kkk) {
                        $res['comp'][$ii]->{$kkk} = $idval[$zzz++];
                    }

                    // disini kita assign perbedaannya dimana aja dan apa aja
                    $exp1 = explode('|', $arr1[$i]['comp']);
                    $exp2 = explode('|', $arr2[$j]['comp']);

                    $ccind = 0;
                    $cczz = 0;
                    $diffarr = [];
                    foreach ($compex as $ccc) {
                        if (!isset($exp1[$ccind]) && isset($exp2[$ccind])) $exp1[$ccind] = "null";
                        if (isset($exp1[$ccind]) && !isset($exp2[$ccind])) $exp2[$ccind] = "null";

                        // echo '-----compare '.$exp1[$ccind].' with '.$exp2[$ccind].'<br/>';
                        if (strcmp($exp1[$ccind], $exp2[$ccind])) {
                            // echo '--------dif<br/>';
                            $diffarr[0][$cczz] = $exp1[$ccind];
                            $diffarr[1][$cczz] = $exp2[$ccind];
                            $diffarr[2][$cczz++] = $ccc;
                        }
                        $ccind++;
                    }

                    $res['comp'][$ii]->{$db2} = $diffarr[0];
                    $res['comp'][$ii]->{$db1} = $diffarr[1];
                    $res['comp'][$ii]->diff = $diffarr[2];

                    $res['comp'][$ii++]->keyz = $arr1[$i]['key'];
                }
                $i++;
                $j++;
            }
        }

        while ($i < count($arr1)) {
            $res['col'][$x++] = $arr1[$i];
            $i++;
        }
        while ($j < count($arr2)) {
            $res['col'][$x++] = $arr2[$j];
            $j++;
        }

        return $res;
    }
}
