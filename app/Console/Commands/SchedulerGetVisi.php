<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetVisi extends Command
{
    protected $signature = "scheduler:getVisi {year} {url}";
    protected $description = "Update Visi Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get Visi from Krisna' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_visi', $this->argument('year'));

            if ($data->success) {
                Log::info('Get data success');

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_visi')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);
                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_visi', $this->argument('year'), $d);
                    }
                    $totalData++;

                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $delz = DB::connection('dbref')->table('t_visi')
                            ->where('kddept', $d->kddept)
                            ->where('kdvisi', $d->kdvisi)
                            ->delete();
                        $del++;
                    } else if ($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) {
                        DB::connection('dbref')->table('t_visi')->upsert(
                            [
                                'kddept' => $d->kddept, 'kdvisi' => $d->kdvisi, 'nmvisi' => $d->nmvisi,
                                'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now()
                            ],
                            ['kddept', 'kdvisi'],
                            ['nmvisi', 'kddept', 'kdvisi', 'kdupdate' => 'U', 'updater', 'tglupdate']
                        );
                        $upsert++;
                    } else {
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_visi')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get Visi from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
