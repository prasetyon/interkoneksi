<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetPriority extends Command
{
    protected $signature = "scheduler:getPriority {year} {url}";
    protected $description = "Update National Priority Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get priority from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            // dd($data);

            if ($data->success) {
                Log::info('Get data success');

                $returnCode = count($data->data);
                foreach ($data->data as $key => $dataprio) {
                    // echo $dataprio->pn.' '.$dataprio->pp.' '.$dataprio->kp.' '.$dataprio->ppn.'<br/>';

                    $kdpn = substr($dataprio->pn, 0, strpos($dataprio->pn, '-'));
                    $nmpn = substr($dataprio->pn, strpos($dataprio->pn, '-') + 1);
                    $kdpp = substr($dataprio->pp, 0, strpos($dataprio->pp, '-'));
                    $nmpp = substr($dataprio->pp, strpos($dataprio->pp, '-') + 1);
                    $kdkp = substr($dataprio->kp, 0, strpos($dataprio->kp, '-'));
                    $nmkp = substr($dataprio->kp, strpos($dataprio->kp, '-') + 1);
                    $kdppn = substr($dataprio->ppn, 0, strpos($dataprio->ppn, '-'));
                    $nmppn = substr($dataprio->ppn, strpos($dataprio->ppn, '-') + 1);

                    // echo $kdpn.'|'.$nmpn.'<br/>';
                    // echo $kdpp.'|'.$nmpp.'<br/>';
                    // echo $kdkp.'|'.$nmkp.'<br/>';
                    // echo $kdppn.'|'.$nmppn.'<br/><br/>';

                    // update prinas
                    DB::connection('dbref')->table('t_prinas')->upsert(
                        ['kdpn' => $kdpn, 'nmpn' => $nmpn],
                        'kdpn',
                        ['nmpn']
                    );

                    // update priprog
                    DB::connection('dbref')->table('t_priprog')->upsert(
                        ['kdpn' => $kdpn, 'kdpp' => $kdpp, 'nmpp' => $nmpp],
                        ['kdpn', 'kdpp'],
                        ['nmpp']
                    );

                    // update prigiat
                    DB::connection('dbref')->table('t_prigiat')->upsert(
                        ['kdpn' => $kdpn, 'kdpp' => $kdpp, 'kdkp' => $kdkp, 'nmkp' => $nmkp],
                        ['kdpn', 'kdpp', 'kdkp'],
                        ['nmkp']
                    );

                    // update priproy
                    DB::connection('dbref')->table('t_priproy')->upsert(
                        ['kdpn' => $kdpn, 'kdpp' => $kdpp, 'kdkp' => $kdkp, 'kdproy' => $kdppn, 'nmproy' => $nmppn],
                        ['kdpn', 'kdpp', 'kdkp', 'kdproy'],
                        ['nmproy']
                    );
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get priority from Krisna');
        return $returnCode;
    }
}
