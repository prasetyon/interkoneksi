<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetOutput extends Command
{
    protected $signature = "scheduler:getOutput {year} {url}";
    protected $description = "Update Output Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get Output from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        $dept = DB::connection('dbref')
            ->table('t_dept')
            ->orderBy('kddept')
            ->get();

        $totalData = $upsert = $del = $stay = 0;

        $map = Helper::getMappingInfo('t_output', $this->argument('year'));

        if ($map->delete_remain == 1 && $map->updater == 1) {
            DB::connection('dbref')->table('t_output')
                ->update(['kdupdate' => 'Z']);
        }

        foreach ($dept as $dep) {
            Log::info('Get output form kddept: ' . $dep->kddept);
            curl_setopt($curl, CURLOPT_URL, $this->argument('url') . '&kddept=' . $dep->kddept);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $response = curl_exec($curl);

            $err = curl_error($curl);

            if ($err) {
                echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
                echo "cURL Error #:" . $err;
                return -1;
            } else {
                $data = json_decode($response);

                if ($data->success) {
                    Log::info('Get data success');

                    if ($this->argument('year') > 2020) {
                        $kro = DB::connection('dbref')->table('t_kro')->select('kdoutput')->get();
                        $i = 0;
                        foreach ($kro as $kk) {
                            $arrKro[$i++] = $kk->kdoutput;
                        }
                    }

                    $returnCode = count($data->data);

                    foreach ($data->data as $key => $d) {
                        if ($key == 0) {
                            Helper::updateKrisnaColumn('t_output', $this->argument('year'), $d);
                        }
                        $totalData++;

                        if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                            $whereArray = array('kdgiat' => $d->kdgiat, 'kdoutput' => $d->kdoutput);

                            $query = DB::connection('dbref')->table('t_output');

                            foreach ($whereArray as $field => $value) {
                                $query->where($field, $value);
                            }

                            $query->delete();
                            $del++;
                        } else if (($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) /*&& in_array($d->kdoutput, $arrKro)*/) {
                            // $var = explode(", ", substr($d->satuan, 1, strlen($d->satuan) - 2));
                            if(intval($this->argument('year')) >= 2023){
                                DB::connection('dbref')->table('t_output')->upsert(
                                    [
                                        'kdgiat' => $d->kdgiat, 'kdoutput' => $d->kdoutput, 'kddept' => $d->kddept,
                                        'kdunit' => $d->kdunit, 'kdprogram' => $d->kdprogram,
                                        'nmoutput' => substr($d->nmoutput, 0, 250), 'sat' => $d->satuan,
                                        // 'sat' => str_replace('"', '', $var[0]),
                                        'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(), 
                                        'thang' => $this->argument('year'), 'kdsum' => '1',
                                        'submitted' => $d->submitted, 'approved' => $d->approved, 'suspended' => $d->suspended
                                    ],
                                    [
                                        'kdgiat', 'kdoutput', 'thang'
                                    ],
                                    [
                                        'nmoutput', 'sat', 'kddept', 'kdunit', 'kdprogram', 
                                        'kdupdate' => 'U', 'updater', 'tglupdate', 'thang',
                                        'submitted', 'approved', 'suspended'
                                    ]
                                );
                            } else {
                                DB::connection('dbref')->table('t_output')->upsert(
                                    [
                                        'kdgiat' => $d->kdgiat, 'kdoutput' => $d->kdoutput, 'kddept' => $d->kddept,
                                        'kdunit' => $d->kdunit, 'kdprogram' => $d->kdprogram,
                                        'nmoutput' => substr($d->nmoutput, 0, 250), 'sat' => $d->satuan,
                                        // 'sat' => str_replace('"', '', $var[0]),
                                        'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(), 
                                        'thang' => $this->argument('year'), 'kdsum' => '1',
                                    ],
                                    [
                                        'kdgiat', 'kdoutput', 'thang'
                                    ],
                                    [
                                        'nmoutput', 'sat', 'kddept', 'kdunit', 'kdprogram', 
                                        'kdupdate' => 'U', 'updater', 'tglupdate', 'thang',
                                    ]
                                );
                            }
                            $upsert++;
                        } else {
                            $stay++;
                        }
                    }
                } else {
                    Log::info('Get data erorr with log: ' . $data->message);
                    return -1;
                }
            }
        }

        if ($map->delete_remain == 1 && $map->updater == 1) {
            $delz = DB::connection('dbref')->table('t_output')
                ->where('kdupdate', 'Z')->delete();
            $del += $delz;
            $stay -= $delz;
        }

        curl_close($curl);

        Log::info('Finished running scheduler get Output from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);
        return $returnCode;
    }
}
