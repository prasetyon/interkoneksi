<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SchedulerGetKomponen extends Command
{
    protected $signature = "scheduler:getKomponen {year} {url}";
    protected $description = "Update Komponen Data";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Log::info('Running scheduler get Komponen from Krisna ' . $this->argument('year'));
        date_default_timezone_set('Asia/Jakarta');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->argument('url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $err = curl_error($curl);
        $totalData = $upsert = $del = $stay = 0;

        if ($err) {
            echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            echo "cURL Error #:" . $err;
            return -1;
        } else {
            $data = json_decode($response);
            $map = Helper::getMappingInfo('t_kmpnen', $this->argument('year'));
            // dd($data);

            if ($data->success) {
                Log::info('Get data success');
                $kro = DB::connection('dbref')->table('t_kro')->select('kdoutput')->get();
                $i = 0;
                foreach ($kro as $kk) {
                    $arrKro[$i++] = $kk->kdoutput;
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    DB::connection('dbref')->table('t_kmpnen')
                        ->update(['kdupdate' => 'Z']);
                }

                $returnCode = count($data->data);

                foreach ($data->data as $key => $d) {
                    if ($key == 0) {
                        Helper::updateKrisnaColumn('t_kmpnen', $this->argument('year'), $d);
                    }
                    $totalData++;
                    if ($map->delete == 1 && property_exists($d, 'suspended') && $d->suspended == 1 && $d->approved == 1) {
                        $whereArray = array(
                            'kddept' => $d->kddept,
                            'kdunit' => $d->kdunit,
                            'kdprogram' => $d->kdprogram,
                            'kdgiat' => $d->kdgiat,
                            'kdoutput' => $d->kdoutput,
                            'kdsoutput' => $d->kdsoutput,
                            'kdkmpnen' => $d->kdkmpnen
                        );

                        $query = DB::connection('dbref')->table('t_kmpnen');

                        foreach ($whereArray as $field => $value) {
                            $query->where($field, $value);
                        }

                        $query->delete();
                        $del++;
                    } else if (($map->upsert == 0 || ($map->upsert == 1 && property_exists($d, 'approved') && $d->approved == 1)) && in_array($d->kdoutput, $arrKro)) {
                        if($d->thang >= 2023){
                            DB::connection('dbref')->table('t_kmpnen')->upsert(
                                [
                                    'thang' => $d->thang, 'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat,
                                    'kdsoutput' => $d->kdsoutput, 'kdoutput' => $d->kdoutput, 'nmkmpnen' => substr($d->nmkmpnen, 0, 250), 'kdkmpnen' => $d->kdkmpnen,
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(), 'jenis_komponen' => $d->jenis_komponen,
                                    'submitted' => $d->submitted, 'approved' => $d->approved, 'suspended' => $d->suspended
                                ],
                                ['thang', 'kddept', 'kdunit', 'kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput', 'kdkmpnen'],
                                [
                                    'nmkmpnen', 'kdupdate' => 'U', 'updater', 'tglupdate', 'jenis_komponen',
                                    'submitted', 'approved', 'suspended'
                                ]
                            );
                        } else {
                            DB::connection('dbref')->table('t_kmpnen')->upsert(
                                [
                                    'thang' => $d->thang, 'kddept' => $d->kddept, 'kdunit' => $d->kdunit, 'kdprogram' => $d->kdprogram, 'kdgiat' => $d->kdgiat,
                                    'kdsoutput' => $d->kdsoutput, 'kdoutput' => $d->kdoutput, 'nmkmpnen' => substr($d->nmkmpnen, 0, 250), 'kdkmpnen' => $d->kdkmpnen,
                                    'kdupdate' => 'C', 'updater' => 'Scheduler WS', 'tglupdate' => now(), 'jenis_komponen' => $d->jenis_komponen
                                ],
                                [
                                    'thang', 'kddept', 'kdunit', 'kdprogram', 'kdgiat', 'kdoutput', 'kdsoutput', 'kdkmpnen'
                                ],
                                [
                                    'nmkmpnen', 'kdupdate' => 'U', 'updater', 'tglupdate', 'jenis_komponen'
                                ]
                            );
                        }
                        $upsert++;
                    } else {
                        // Log::info('stay: ' . $d->kddept . ' ' . $d->kdunit . ' ' . $d->kdprogram . ' ' . $d->kdgiat . ' ' . $d->kdsoutput . ' ' . $d->kdoutput);
                        $stay++;
                    }
                }

                if ($map->delete_remain == 1 && $map->updater == 1) {
                    $delz = DB::connection('dbref')->table('t_kmpnen')
                        ->where('kdupdate', 'Z')->delete();
                    $del += $delz;
                    $stay -= $delz;
                }
            } else {
                Log::info('Get data erorr with log: ' . $data->message);
                return -1;
            }
        }

        curl_close($curl);

        Log::info('Finished running scheduler get Komponen from Krisna');
        $rep = 'Total: ' . $totalData . ' Upsert: ' . $upsert . ' Delete: ' . $del . ' Stay: ' . $stay;
        Log::info($rep);

        return $returnCode;
    }
}
